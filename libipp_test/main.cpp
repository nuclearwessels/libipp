

#include "libipp.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <vector>
#include <iostream>

using namespace libipp;
using namespace std;

//template <class ImageType, class PrimitiveType>
//bool CompareValue(const ImageType& img, PrimitiveType value)
//{
//    for (int y = 0; y < img.height(); y++)
//    {
//        PrimitiveType* p = img.ptr(0, y);
//        for (int x = 0; x < img.width(); x++)
//        {
//            if (*p != value)
//                return false;
//        }
//    }
//
//    return true;
//}
//
//
//template <class ImageType, class PrimitiveType>
//bool CompareImagesEqual(const ImageType& src1, const ImageType& src2)
//{
//    const PrimitiveType True = (PrimitiveType) -1;
//
//    Validate(src1.width() == src2.width());
//    Validate(src1.height() == src2.height());
//
//    //dst.compare(dst.rect(), result, MakePoint(0, 0), compare, MakePoint(0, 0), ippCmpEq);
//    ImageType compare(src1.width(), src1.height());
//    src1.compare(src1.rect(), src2, MakePoint(0, 0), compare, MakePoint(0, 0), ippCmpEq);
//
//    PrimitiveType minValue;
//    compare.min(compare.rect(), minValue);
//    return minValue == True;
//}




TEST_CASE("CpuInfo")
{
    vector<string> features;
    libipp::GetCpuFeatures(features);

    string cpuInfo = libipp::GetCpuInfo();
    cout << "CPU info: " << cpuInfo << endl;
}


TEST_CASE("Ipp8uC1_Malloc")
{
    Ipp8uC1 img(8, 4);
    
    initializer_list<Ipp8u> a =
    {
        5, 4, 3, 4, 5, 8, 8, 8,
        3, 2, 1, 2, 3, 8, 8, 8,
        3, 2, 1, 2, 3, 8, 8, 8,
        5, 4, 3, 4, 5, 8, 8, 8
    };

    img.init(a);

    //string s = img.str();
    //cout << s;
}


TEST_CASE("Ipp8uC1_CopyMask")
{
    Ipp8uC1 src(5, 4);
    src.set(3, src.rect());

    Ipp8uC1 mask(3, 3);
    mask.set(3, mask.rect());
    *mask.ptr(0, 0) = 0;

    Ipp8uC1 dst(src.width(), src.height());
    dst.set(0, dst.rect());

    src.copyMask(mask.rect(), dst, MakePoint(0, 0), mask, MakePoint(0, 0));

    initializer_list<Ipp8u> resultList =
    {
        0, 3, 3, 0, 0,
        3, 3, 3, 0, 0,
        3, 3, 3, 0, 0,
        0, 0, 0, 0, 0
    };
    Ipp8uC1 result(5, 4);
    result.init(resultList);

    bool ok = (dst == result);
    REQUIRE(ok);

    //Ipp8uC1 compare(5, 4);
    //dst.compare(dst.rect(), result, MakePoint(0, 0), compare, MakePoint(0, 0), ippCmpEq);

    //cout << compare.str();

    //bool ok = CompareValue<Ipp8uC1, Ipp8u>(compare, 0xFF);
    //CHECK(ok);
}



TEST_CASE("Ipp8uC1_CopyConstBorder")
{
    Ipp8uC1 img(8, 4);

    initializer_list<Ipp8u> a =
    {
        5, 4, 3, 4, 5, 8, 8, 8,
        3, 2, 1, 2, 3, 8, 8, 8,
        3, 2, 1, 2, 3, 8, 8, 8,
        5, 4, 3, 4, 5, 8, 8, 8
    };

    img.init(a);

}




//int main()
//{
//    return UnitTest::RunAllTests();
//}
