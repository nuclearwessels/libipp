
#pragma once

#include "libippcore.h"

#include "ipp8uc3.h"
#include "ipp8uc4.h"
#include "ippscpp.h"


namespace libipp
{

class Ipp8uC3;
class Ipp8uC4;

class Ipp8uC1 : public IppImgBase<Ipp8u>
{
public:
    Ipp8uC1(int w, int h);

    Ipp8u* ptr(int x = 0, int y = 0) const;
    Ipp8u* ptr(const IppiPoint& pt) const;

    bool operator == (Ipp8u value) const;
    bool operator == (const Ipp8uC1& src2) const;

    void set(Ipp8u value, const IppiRect& roi);
    void setMask(Ipp8u value, const IppiRect& roi, const Ipp8uC1& mask, const IppiPoint& maskPt);

    void convert16uC1(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt) const;
    void convert16sC1(const IppiRect& srcRect, Ipp16sC1& dst, const IppiPoint& dstPt) const;
    void convert32sC1(const IppiRect& srcRect, Ipp32sC1& dst, const IppiPoint& dstPt) const;
    void convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const;
    Ipp32fC1 convert32fC1(const IppiRect& srcRect) const;

    void scale16uC1(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt) const;
    void scale16sC1(const IppiRect& srcRect, Ipp16sC1& dst, const IppiPoint& dstPt) const;
    void scale32sC1(const IppiRect& srcRect, Ipp32sC1& dst, const IppiPoint& dstPt) const;
    void scale32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt, Ipp32f min, Ipp32f max) const;

    void copy(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt) const;
    void copyMask(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const;
    void copyC3(const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt) const;
    void copyConstBorder(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect, 
        int topBorderHeight, int bottomBorderHeight, Ipp8u value) const;
    void copyReplicateBorder(const IppiRect& srcRect, const IppiSize& dstSize, int topBorderHeight, int leftBorderWidth);
    void copyReplicateBorder(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect,
        int topBorderHeight, int leftBorderWidth) const;
    // void copyWrapBorder

    /// Copy pixel values with subpixel precision without conversion.
    /// \sa ippiCopySubpix_8u_C1R
    void copySubpix(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy) const;   
    
    /// Copy pixel values with subpixel precision with conversion to float.
    /// \sa ippiCopySubpix_8u32f_C1R
    void copySubpix(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy) const;
    
    /// Copy pixel values with subpixel precision with conversion to integer.
    /// \sa ippiCopySubpix_8u16u_C1R_Sfs
    void copySubpix(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy, int scaleFactor = 0) const;


    /// Copies pixel values of the intersection with specified window with subpixel precision without conversion.
    /// \sa ippiCopySubpixIntersect_8u_C1R
    void copySubpixIntersect(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect,
        const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max) const;

    /// Copies pixel values of the intersection with specified window with subpixel precision with conversion to float.
    /// \sa ippiCopySubpixIntersect_8u32f_C1R
    void copySubpixIntersect(const IppiRect& srcRect, Ipp32fC1& dst, const IppiRect& dstRect, 
        const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max) const;

    /// Copy Copies pixel values of the intersection with specified window with subpixel precision with conversion to integer.
    /// \sa ippiCopySubpixIntersect_8u16u_C1R_Sfs
    void copySubpixIntersect(const IppiRect& srcRect, Ipp16uC1& dst, const IppiRect& dstRect, 
        const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max, int scaleFactor = 0) const;

	void dupC3(const IppiPoint& pt, Ipp8uC3& dst, const IppiPoint& dstPt, const IppiSize& size) const;
	void dupC4(const IppiPoint& pt, Ipp8uC4& dst, const IppiPoint& dstPt, const IppiSize& size) const;

	void transpose(const IppiRect& roi);
	void transpose(const IppiPoint& srcPt, Ipp8uC1& dst, const IppiPoint& dstPt, const IppiSize& size) const;

	void addRandUniform(const IppiRect& roi, Ipp8u low, Ipp8u high, unsigned int seed);
	void addRandGauss(const IppiRect& roi, Ipp8u mean, Ipp8u stDev, unsigned int seed);
	void imageJaehne(const IppiRect& roi);
	void imageRamp(const IppiRect& roi, float offset, float slope, IppiAxis axis);
	void sampleLine(const IppiRect& roi, Ipps8u& dst, int dstOffset, const IppiPoint& pt1, const IppiPoint& pt2) const;


    void min(const IppiRect& rect, Ipp8u& minValue) const;
    void min(const IppiRect& rect, Ipp8u& minValue, int& indexX, int& indexY) const;
    void max(const IppiRect& rect, Ipp8u& maxValue) const;
    void max(const IppiRect& rect, Ipp8u& maxValue, int& indexX, int& indexY) const;

    void mirror(const IppiRect& roi, IppiAxis axis);
    void mirror(const IppiRect& srcROI, Ipp8uC1& dst, const IppiPoint& dstPt, IppiAxis axis) const;

    //! Adds a constant to the specified ROI.
    void add(Ipp8u value, const IppiRect& rect, int scaleFactor = 0);
    void add(Ipp8u value, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, int scaleFactor = 0);

    //! Add a second image to this image.
    void add(const IppiRect& srcDstRect, const Ipp8uC1& src2, const IppiPoint& src2Pt, int scaleFactor = 0);

    //! Add two images to a third
    static void add(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt, int scaleFactor = 0);

	//! ippiAnd_8u_C1IR
	void and(const IppiPoint& srcDstPt, const Ipp8uC1& src, const IppiRect& srcRect);

    //! ippiAnd_8u_C1R
    static void and(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt);

    //! ippiAndC_8u_C1R
    static void andC(const Ipp8uC1& src, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, Ipp8u value);

    //! ippiAndC_8u_C1IR
    void andC(Ipp8u value, const IppiRect& srcDstRect);


    //! ippiCompare_8u_C1R
    static void compare(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt,
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp);

    //! ippiCompare_8u_C1R
    void compare(const IppiRect& srcRect, const Ipp8uC1& src2, const IppiPoint& src2Pt,
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;

    // ippiCompareC_8u_C1R
    static void compareC(const Ipp8uC1& src1, const IppiRect& src1Rect, Ipp8u value,
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp);

    //! ippiCompareC_8u_C1R
    void compareC(const IppiRect& srcRect, Ipp8u value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;

    void countInRange(const IppiRect& rect, int& count, Ipp8u lowerBound, Ipp8u upperBound) const;


    void dilateBorder(const IppiRect& rect, const Ipp8uC1& mask, Ipp8uC1& dst, const IppiPoint& dstPt,
        IppiBorderType borderType, Ipp8u borderValue) const;


    void and(const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt);
    void or (const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt);
    void xor(const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt);
    void not(const IppiRect& srcDstRect);

    void andC(const IppiRect& srcDstRect, Ipp8u value);
    void orC(const IppiRect& srcDstRect, Ipp8u value);
    void xorC(const IppiRect& srcDstRect, Ipp8u value);

    void minMax(const IppiRect& rect, Ipp8u& minValue, Ipp8u& maxValue) const;
    void minMax(const IppiRect& rect, Ipp8u& minValue, Ipp8u& maxValue, IppiPoint& minIndexPt, IppiPoint& maxIndexPt) const;

    void mean(const IppiRect& rect, Ipp64f& mean) const;
    void mean(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean) const;

    void meanStdDev(const IppiRect& rect, Ipp64f& mean, Ipp64f& stdDev) const;
    void meanStdDev(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean, Ipp64f& stdDev) const;

    void qualityIndex(const IppiPoint& pt1, const Ipp8uC1& src2, const IppiRect& rect2, Ipp32f qualityIndex[1]) const;
    void qualityIndex(const IppiPoint& pt1, const Ipp8uC1& src2, const IppiRect& rect2, Ipp32f qualityIndex[1], Ipps8u& workBuffer) const;
    static void qualityIndex(const Ipp8uC1& src1, const IppiPoint& pt1, const Ipp8uC1& src2, const IppiPoint& pt2, const IppiSize& size, Ipp32f qualityIndex[1]);
    static void qualityIndex(const Ipp8uC1& src1, const IppiPoint& pt1, const Ipp8uC1& src2, const IppiPoint& pt2, const IppiSize& size, Ipp32f qualityIndex[1], Ipps8u& workBuff);
    int getQualityIndexBufferSize() const;


    Ipp8uC1 remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation = IPPI_INTER_NN) const;
    void remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC1& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN) const;
    static void remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC1& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN);
};

}

