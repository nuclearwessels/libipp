
#pragma once

#include "libippcore.h"

#include "ipp8uc1.h"
#include "ipp16uc1.h"

namespace libipp
{


class Ipp32fC1 : public IppImgBase<Ipp32f>
{
public:
    Ipp32fC1(int w, int h);

    Ipp32f* ptr(int x = 0, int y = 0) const;
    Ipp32f* ptr(const IppiPoint& pt) const;
    
    void copy(const IppiRect& rect, Ipp32fC1& dst, const IppiPoint& dstPt) const;
    Ipp32fC1 copy(const IppiRect& rect) const;

    void copyMask(const IppiRect& rect, Ipp32fC1& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const;
    Ipp32fC1 copyMask(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt) const;

    bool operator == (Ipp32f value) const;
    bool operator == (const Ipp32fC1& src2) const;

    void set(float value);
    void set(float value, const IppiRect& dstRect);

    void setMask(Ipp32f value, const IppiRect& roi, const Ipp8uC1& mask, const IppiPoint& maskPt);

    //! Performs an inplace absolute value.
    void abs(const IppiRect& srcRect);

    //! Computes absolute value to destination
    void abs(const IppiRect& srcRect, Ipp32fC1& dst, const IppiRect& dstRect) const;

    //! Adds a constant to the specified ROI.
    void add(float value, const IppiRect& rect);

    //! Add a second image to this image.
    void add(const IppiRect& srcDstRect, const Ipp32fC1& src2, const IppiPoint& src2Pt);

    //! Add two images to a third
    static void add(const Ipp32fC1& src1, const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& srcPt2, Ipp32fC1& dst, const IppiPoint& dstPt);

    //! ippiAddC_32f_C1IR
    void addC(const IppiRect& rect, Ipp32f value);
    
    void sub(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt);

    void subC(const IppiRect& rect, Ipp32f value);
    
    void mul(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt);

    void mulC(const IppiRect& rect, Ipp32f value);
    
    void div(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt);
    
    void divC(const IppiRect& rect, Ipp32f value);

    //! ippiCompare_32f_C1R
    static void compare(const Ipp32fC1& src1, const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, 
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp);
    
    //! ippiCompare_32f_C1R
    void compare(const IppiRect& srcRect, const Ipp32fC1& src2, const IppiPoint& src2Pt, 
                 Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;

    // ippiCompareC_32f_C1R
    static void compareC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value, 
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp);

    //! ippiCompareC_32f_C1R
    void compareC(const IppiRect& srcRect, Ipp32f value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;


    static void compareEqualEps(const Ipp32fC1& src1, const IppiRect& src1Rect,
                                const Ipp32fC1& src2, const IppiPoint& src2Pt,
                                Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps);
    static Ipp8uC1 compareEqualEps(const Ipp32fC1& src1, const IppiRect& src1Rect,
                                   const Ipp32fC1& src2, const IppiPoint& src2Pt,
                                   Ipp32f eps);

    void compareEqualEps(const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, 
                         Ipp8uC1& dst, const IppiPoint& dstPt, 
                         Ipp32f eps) const;
    Ipp8uC1 compareEqualEps(const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp32f eps) const;

    static void compareEqualEpsC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value,
                                 Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps);
    static Ipp8uC1 compareEqualEpsC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value, Ipp32f eps);

    void compareEqualEpsC(const IppiRect& src1Rect, Ipp32f value,  
                          Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps) const;
    Ipp8uC1 compareEqualEpsC(const IppiRect& src1Rect, Ipp32f value, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp32f eps) const;


    static void convert16u(const Ipp32fC1& src, const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode = ippRndNear);
    void convert16u(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode = ippRndNear) const;
    Ipp16uC1 convert16u(const IppiRect& rect, IppRoundMode rndMode = ippRndNear) const;

    static void convert8u(const Ipp32fC1& src, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode = ippRndNear);
    void convert8u(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode = ippRndNear) const;
    Ipp8uC1 convert8u(const IppiRect& rect, IppRoundMode rndMode = ippRndNear) const;


    void countInRange(const IppiRect& rect, int& count, Ipp32f lowerBound, Ipp32f upperBound) const;


    void dilateBorder(const IppiRect& rect, const Ipp8uC1& mask, Ipp32fC1& dst, const IppiPoint& dstPt, 
        IppiBorderType borderType, Ipp32f borderValue) const;

    void max(const IppiRect& srcRect, Ipp32f& maxValue) const;
    void min(const IppiRect& srcRect, Ipp32f& minValue) const;

    void max(const IppiRect& srcRect, Ipp32f& maxValue, int& indexX, int& indexY) const;
    void min(const IppiRect& srcRect, Ipp32f& minValue, int& indexX, int& indexY) const;

    void minMax(const IppiRect& rect, Ipp32f& minValue, Ipp32f& maxValue) const;
    void minMax(const IppiRect& rect, Ipp32f& minValue, Ipp32f& maxValue, IppiPoint& minIndexPt, IppiPoint& maxIndexPt) const;

    void mean(const IppiRect& rect, Ipp64f& mean, IppHintAlgorithm hint = ippAlgHintAccurate) const;
    void mean(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean) const;

    void meanStdDev(const IppiRect& rect, Ipp64f& mean, Ipp64f& stdDev) const;
    void meanStdDev(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean, Ipp64f& stdDev) const;

    Ipp8uC1 scale8u(Ipp32f vMin, Ipp32f vMax) const;
    void scale8u(const IppiPoint& srcPt, Ipp8uC1& dst, const IppiPoint& dstPt, const IppiSize& size, Ipp32f vMin, Ipp32f vMax) const;


private:

};

}