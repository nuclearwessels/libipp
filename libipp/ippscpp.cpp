
#include "ipps.h"
#include "ippscpp.h"
#include "libippcore.h"


using namespace std;
using namespace libipp;



void IppSignalDeleter::operator()(void * ippsBuff)
{
    ippsFree(ippsBuff);
}



Ipps8s::Ipps8s(int size)
{
	Ipp8s* buff = ippsMalloc_8s(size);
	if (buff == nullptr)
		throw IppException("ippsMalloc_8s failed");

	buffer_ = unique_ptr<Ipp8s, IppSignalDeleter>(buff);
}



Ipps8u::Ipps8u(int size)
{
	Ipp8u* buff = ippsMalloc_8u(size);
	if (buff == nullptr)
		throw IppException("ippsMalloc_8u failed");

	buffer_ = unique_ptr<Ipp8u, IppSignalDeleter>(buff);
}





void libipp::Ipps8u::set(Ipp8u value, int offset, int length)
{
	validateOffsetLength(offset, length);

	IppStatus status = ippsSet_8u(value, ptr(offset), length);
	ValidateStatus(status);
}

void Ipps8u::copy(int srcOffset, Ipps8u& dst, int dstOffset, int length)
{
	validateOffsetLength(srcOffset, length);

	dst.validateOffsetLength(dstOffset, length);

	IppStatus status = ippsCopy_8u(ptr(srcOffset), dst.ptr(dstOffset), length);
	ValidateStatus(status);
}


