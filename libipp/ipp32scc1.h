
#pragma once

#include "libippcore.h"


namespace libipp
{


class Ipp32scC1 : public IppImgBase<Ipp32sc>
{
public:
    Ipp32scC1(int w, int h);

    Ipp32sc* ptr(int x = 0, int y = 0) const;
};


}