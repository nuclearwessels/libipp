#pragma once


#include "libippcore.h"

#include "ipp8uc1.h"


namespace libipp
{


class Ipp64fC1 : public IppImgBase<Ipp64f>
{
public:
    Ipp64fC1(int w, int h);

    Ipp64f* ptr(int x, int y) const;
    Ipp64f* ptr(const IppiPoint& pt) const;

    bool Ipp64fC1::operator == (Ipp64f value) const;
    bool Ipp64fC1::operator == (const Ipp64fC1& src2) const;

    void compare(const IppiRect& srcRect, const Ipp64fC1& src2, const IppiPoint& src2Pt,
        Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;

    void compareC(const IppiRect& srcRect, Ipp64f value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const;


};

}
