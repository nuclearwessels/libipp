
#include "ipp32fc1.h"

//#include "ippcv_l.h"

using namespace libipp;
using namespace std;


Ipp32fC1::Ipp32fC1(int w, int h) : IppImgBase(w, h)
{
    Ipp32f* buff = ippiMalloc_32f_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_32f_C1 failed");

    buffer_ = unique_ptr<Ipp32f, IppImgDeleter>(buff);
}



Ipp32f* Ipp32fC1::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 1);
}


Ipp32f* Ipp32fC1::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


bool Ipp32fC1::operator == (Ipp32f value) const
{
    return CompareImageEqualValue<Ipp32fC1, Ipp32f>(*this, value);
}


bool Ipp32fC1::operator == (const Ipp32fC1& src2) const
{
    return CompareImagesEqual<Ipp32fC1, Ipp32f>(*this, src2);
}



void Ipp32fC1::copy(const IppiRect& rect, Ipp32fC1& dst, const IppiPoint& dstPt) const
{
    validateROI(rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_32f_C1R(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect));
    ValidateStatus(status);
}


Ipp32fC1 Ipp32fC1::copy(const IppiRect& rect) const
{
    validateROI(rect);

    Ipp32fC1 dst(rect.width, rect.height);

    IppStatus status = ippiCopy_32f_C1R(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), MakeSize(rect));
    ValidateStatus(status);

    return dst;
}


void Ipp32fC1::copyMask(const IppiRect& rect, Ipp32fC1& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const
{
    validateROI(rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(rect));
    dst.validateROI(dstRect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));
    mask.validateROI(maskRect);

    IppStatus status = ippiCopy_32f_C1MR(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect),
        mask.ptr(maskPt), mask.step());
    ValidateStatus(status);
}


Ipp32fC1 Ipp32fC1::copyMask(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt) const
{
    validateROI(rect);

    Ipp32fC1 dst(rect.width, rect.height);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));

    IppStatus status = ippiCopy_32f_C1MR(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), MakeSize(rect), mask.ptr(maskPt), mask.step());
    ValidateStatus(status);

    return dst;
}



void Ipp32fC1::set(float value)
{
    set(value, MakeRect(0, 0, width_, height_));
}


void Ipp32fC1::set(float value, const IppiRect& dstRect)
{
    validateROI(dstRect);

    IppStatus status = ippiSet_32f_C1R(value, ptr(dstRect.x, dstRect.y), step_, MakeSize(dstRect));
    ValidateStatus(status);
}


void Ipp32fC1::setMask(Ipp32f value, const IppiRect& roi, const Ipp8uC1& mask, const IppiPoint& maskPt)
{
    validateROI(roi);

    IppiRect maskROI = MakeRect(maskPt, MakeSize(roi));
    mask.validateROI(maskROI);

    IppStatus status = ippiSet_32f_C1MR(value, ptr(roi.x, roi.y), step_, MakeSize(roi), mask.ptr(maskPt.x, maskPt.y), mask.step());
    ValidateStatus(status);
}


void Ipp32fC1::abs(const IppiRect& srcRect)
{
    validateROI(srcRect);

    IppStatus status = ippiAbs_32f_C1IR(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp32fC1::abs(const IppiRect& srcRect, Ipp32fC1& dst, const IppiRect& dstRect) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);

    ValidateSameSize(srcRect, dstRect);

    IppStatus status = ippiAbs_32f_C1R(ptr(srcRect.x, srcRect.y), step_,
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp32fC1::add(const IppiRect& srcDstRect, const Ipp32fC1& src2, const IppiPoint& src2Pt)
{
    validateROI(srcDstRect);

    src2.validateROI(MakeRect(src2Pt.x, src2Pt.y, srcDstRect.width, srcDstRect.height));

    IppStatus status = ippiAdd_32f_C1IR(src2.ptr(src2Pt.x, src2Pt.y), src2.step(), ptr(srcDstRect.x, srcDstRect.y),
        step_, MakeSize(srcDstRect));
    ValidateStatus(status);
}



void Ipp32fC1::add(float value, const IppiRect& rect)
{
    validateROI(rect);

    IppStatus status = ippiAddC_32f_C1IR(value, ptr(rect.x, rect.y), step_, MakeSize(rect));
    ValidateStatus(status);
}



void Ipp32fC1::add(const Ipp32fC1& src1, const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp32fC1& dst, const IppiPoint& dstPt)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt.x, src2Pt.y, src1Rect.width, src1Rect.height);
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt.x, dstPt.y, src1Rect.width, src1Rect.height);
    dst.validateROI(dstRect);

    IppStatus status = ippiAdd_32f_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), src2.ptr(src2Pt.x, src2Pt.y),
        src2.step(), dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(src1Rect));
    ValidateStatus(status);
}


void Ipp32fC1::addC(const IppiRect& rect, Ipp32f value)
{
    validateROI(rect);

    IppStatus status = ippiAddC_32f_C1IR(value, ptr(rect.x, rect.y), step(), MakeSize(rect));
    ValidateStatus(status);
}


void Ipp32fC1::sub(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiSub_32f_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp32fC1::subC(const IppiRect& rect, Ipp32f value)
{
    validateROI(rect);

    IppStatus status = ippiSubC_32f_C1IR(value, ptr(rect.x, rect.y), step(), MakeSize(rect));
    ValidateStatus(status);
}


void Ipp32fC1::mul(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiMul_32f_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp32fC1::mulC(const IppiRect& rect, Ipp32f value)
{
    validateROI(rect);

    IppStatus status = ippiMulC_32f_C1IR(value, ptr(rect.x, rect.y), step(), MakeSize(rect));
    ValidateStatus(status);
}




void Ipp32fC1::div(const IppiRect& srcDstRect, const Ipp32fC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiDiv_32f_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp32fC1::divC(const IppiRect& rect, Ipp32f value)
{
    validateROI(rect);

    IppStatus status = ippiDivC_32f_C1IR(value, ptr(rect.x, rect.y), step(), MakeSize(rect));
    ValidateStatus(status);
}




void Ipp32fC1::compare(const Ipp32fC1& src1, const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, 
    Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp)
{
    src1.validateROI(src1Rect);

    IppiSize size = MakeSize(src1Rect);
    IppiRect src2Rect = MakeRect(src2Pt, size);
    IppiRect dstRect = MakeRect(dstPt, size);

    src2.validateROI(src2Rect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompare_32f_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), src2.ptr(src2Pt.x, src2Pt.y), src2.step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), size, cmpOp);
    ValidateStatus(status);
}


void Ipp32fC1::compare(const IppiRect& srcRect, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    IppiSize src1Size = MakeSize(srcRect);

    IppiRect src2Rect = MakeRect(src2Pt, src1Size);
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, src1Size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompare_32f_C1R(ptr(srcRect.x, srcRect.y), step_, 
        src2.ptr(src2Pt.x, src2Pt.y), src2.step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), src1Size, cmpOp);
    ValidateStatus(status);
}



void Ipp32fC1::compareC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value, 
    Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp)
{
    src1.validateROI(src1Rect);


    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareC_32f_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), value,
        dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(src1Rect), cmpOp);
    ValidateStatus(status);
}



void Ipp32fC1::compareC(const IppiRect& srcRect, Ipp32f value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    IppiSize srcSize = MakeSize(srcRect);

    IppiRect dstRect = MakeRect(dstPt, srcSize);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareC_32f_C1R(ptr(srcRect.x, srcRect.y), step(), value, 
                                                dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect), cmpOp);
    ValidateStatus(status);
}


void Ipp32fC1::compareEqualEps(const Ipp32fC1& src1, const IppiRect& src1Rect,
    const Ipp32fC1& src2, const IppiPoint& src2Pt,
    Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareEqualEps_32f_C1R(src1.ptr(MakePoint(src1Rect)), src1.step(), src2.ptr(src2Pt), src2.step(),
        dst.ptr(dstPt), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);
}


Ipp8uC1 Ipp32fC1::compareEqualEps(const Ipp32fC1& src1, const IppiRect& src1Rect,
    const Ipp32fC1& src2, const IppiPoint& src2Pt,
    Ipp32f eps)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    Ipp8uC1 dst(src1Rect.width, src1Rect.height);

    IppStatus status = ippiCompareEqualEps_32f_C1R(src1.ptr(MakePoint(src1Rect)), src1.step(), src2.ptr(src2Pt), src2.step(),
        dst.ptr(), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);

    return std::move(dst);
}


void Ipp32fC1::compareEqualEps(const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, 
                               Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps) const
{
    validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareEqualEps_32f_C1R(ptr(MakePoint(src1Rect)), step(), src2.ptr(src2Pt), src2.step(),
        dst.ptr(dstPt), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);
}


Ipp8uC1 Ipp32fC1::compareEqualEps(const IppiRect& src1Rect, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp32f eps) const
{
    validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    Ipp8uC1 dst(src1Rect.x, src2Rect.y);

    IppStatus status = ippiCompareEqualEps_32f_C1R(ptr(MakePoint(src1Rect)), step(), src2.ptr(src2Pt), src2.step(),
        dst.ptr(), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);

    return std::move(dst);
}


void Ipp32fC1::compareEqualEpsC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value,
                                Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps)
{
    src1.validateROI(src1Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareEqualEpsC_32f_C1R(src1.ptr(MakePoint(src1Rect)), src1.step(), value, 
        dst.ptr(dstPt), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);
}


Ipp8uC1 Ipp32fC1::compareEqualEpsC(const Ipp32fC1& src1, const IppiRect& src1Rect, Ipp32f value, Ipp32f eps)
{
    src1.validateROI(src1Rect);

    Ipp8uC1 dst(src1Rect.width, src1Rect.height);

    IppStatus status = ippiCompareEqualEpsC_32f_C1R(src1.ptr(MakePoint(src1Rect)), src1.step(), value, 
        dst.ptr(), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);

    return std::move(dst);
}


void Ipp32fC1::compareEqualEpsC(const IppiRect& src1Rect, Ipp32f value, 
                                Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f eps) const
{
    validateROI(src1Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareEqualEpsC_32f_C1R(ptr(MakePoint(src1Rect)), step(), value,
        dst.ptr(), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);
}


Ipp8uC1 Ipp32fC1::compareEqualEpsC(const IppiRect& src1Rect, Ipp32f value, const Ipp32fC1& src2, const IppiPoint& src2Pt, Ipp32f eps) const
{
    validateROI(src1Rect);

    Ipp8uC1 dst(src1Rect.width, src1Rect.height);

    IppStatus status = ippiCompareEqualEpsC_32f_C1R(ptr(MakePoint(src1Rect)), step(), value,
        dst.ptr(), dst.step(), MakeSize(src1Rect), eps);
    ValidateStatus(status);

    return std::move(dst);
}





void Ipp32fC1::convert16u(const Ipp32fC1& src, const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode)
{
    src.validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32f16u_C1R(src.ptr(srcRect.x, srcRect.y), src.step(), dst.ptr(dstPt), dst.step(), size, rndMode);
    ValidateStatus(status);
}


void Ipp32fC1::convert16u(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32f16u_C1R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(dstPt), dst.step(), size, rndMode);
    ValidateStatus(status);
}


Ipp16uC1 Ipp32fC1::convert16u(const IppiRect& rect, IppRoundMode rndMode) const
{
    validateROI(rect);

    IppiSize size = MakeSize(rect);

    Ipp16uC1 dst(rect.width, rect.height);

    IppStatus status = ippiConvert_32f16u_C1R(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), size, rndMode);
    ValidateStatus(status);

    return std::move(dst);
}



void Ipp32fC1::convert8u(const Ipp32fC1& src, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode)
{
    src.validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32f8u_C1R(src.ptr(srcRect.x, srcRect.y), src.step(), dst.ptr(dstPt), dst.step(), size, rndMode);
    ValidateStatus(status);
}


void Ipp32fC1::convert8u(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, IppRoundMode rndMode) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32f8u_C1R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(dstPt), dst.step(), size, rndMode);
    ValidateStatus(status);
}


Ipp8uC1 Ipp32fC1::convert8u(const IppiRect& rect, IppRoundMode rndMode) const
{
    validateROI(rect);

    IppiSize size = MakeSize(rect);

    Ipp8uC1 dst(rect.width, rect.height);

    IppStatus status = ippiConvert_32f8u_C1R(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), size, rndMode);
    ValidateStatus(status);

    return std::move(dst);
}



void Ipp32fC1::countInRange(const IppiRect& rect, int& count, Ipp32f lowerBound, Ipp32f upperBound) const
{
    validateROI(rect);

    IppStatus status = ippiCountInRange_32f_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &count, lowerBound, upperBound);
    ValidateStatus(status);
}



#if IPP_VERSION_MAJOR > 7

void Ipp32fC1::dilateBorder(const IppiRect& rect, const Ipp8uC1& mask, Ipp32fC1& dst, const IppiPoint& dstPt,
    IppiBorderType borderType, Ipp32f borderValue) const
{
    int specSize = 0; 
    int bufferSize = 0;
    
    IppStatus status = ippiMorphologyBorderGetSize_32f_C1R(MakeSize(rect), mask.size(), &specSize, &bufferSize);
    ValidateStatus(status);

    unique_ptr<IppiMorphState, IppSignalDeleter> morphState( (IppiMorphState*)ippsMalloc_8u(specSize));
    unique_ptr<Ipp8u, IppSignalDeleter> workBuffer( ippsMalloc_8u(bufferSize) );

    status = ippiMorphologyBorderInit_32f_C1R(MakeSize(rect), mask.ptr(), mask.size(), morphState.get(), workBuffer.get());
    ValidateStatus(status);

    status = ippiDilateBorder_32f_C1R(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect), borderType, borderValue,
        morphState.get(), workBuffer.get() );
    ValidateStatus(status);
}

#endif


void Ipp32fC1::max(const IppiRect& srcRect, Ipp32f& maxValue) const
{
    validateROI(srcRect);

    IppStatus status = ippiMax_32f_C1R(ptr(srcRect.x, srcRect.y), step(), MakeSize(srcRect), &maxValue);
    ValidateStatus(status);
}


void Ipp32fC1::min(const IppiRect& srcRect, Ipp32f& minValue) const
{
    validateROI(srcRect);

    IppStatus status = ippiMax_32f_C1R(ptr(srcRect.x, srcRect.y), step(), MakeSize(srcRect), &minValue);
    ValidateStatus(status);
}


void Ipp32fC1::max(const IppiRect& srcRect, Ipp32f& maxValue, int& indexX, int& indexY) const
{
    validateROI(srcRect);

    IppStatus status = ippiMaxIndx_32f_C1R(ptr(srcRect.x, srcRect.y), step(), MakeSize(srcRect), &maxValue, &indexX, &indexY);
    ValidateStatus(status);
}


void Ipp32fC1::min(const IppiRect& srcRect, Ipp32f& minValue, int& indexX, int& indexY) const
{
    validateROI(srcRect);

    IppStatus status = ippiMaxIndx_32f_C1R(ptr(srcRect.x, srcRect.y), step(), MakeSize(srcRect), &minValue, &indexX, &indexY);
    ValidateStatus(status);
}


void Ipp32fC1::mean(const IppiRect& rect, Ipp64f& mean, IppHintAlgorithm hint) const
{
    validateROI(rect);

    IppStatus status = ippiMean_32f_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &mean, hint);
    ValidateStatus(status);
}



void Ipp32fC1::mean(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean) const
{
    validateROI(rect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));
    mask.validateROI(maskRect);

    IppStatus status = ippiMean_32f_C1MR(ptr(rect.x, rect.y), step(), mask.ptr(maskPt), mask.step(), MakeSize(maskRect), &mean);
    ValidateStatus(status);
}



void Ipp32fC1::meanStdDev(const IppiRect& rect, Ipp64f& mean, Ipp64f& stdDev) const
{
    validateROI(rect);

    IppStatus status = ippiMean_StdDev_32f_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &mean, &stdDev);
    ValidateStatus(status);
}



void Ipp32fC1::meanStdDev(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean, Ipp64f& stdDev) const
{
    validateROI(rect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));
    mask.validateROI(maskRect);

    IppStatus status = ippiMean_StdDev_32f_C1MR(ptr(rect.x, rect.y), step(), mask.ptr(maskPt), mask.step(), MakeSize(rect), &mean, &stdDev);
    ValidateStatus(status);
}



void Ipp32fC1::minMax(const IppiRect& rect, Ipp32f& minValue, Ipp32f& maxValue) const
{
    validateROI(rect);

    IppStatus status = ippiMinMax_32f_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &minValue, &maxValue);
    ValidateStatus(status);
}



void Ipp32fC1::minMax(const IppiRect& rect, Ipp32f& minValue, Ipp32f& maxValue, IppiPoint& minIndexPt, IppiPoint& maxIndexPt) const
{
    validateROI(rect);

    IppStatus status = ippiMinMaxIndx_32f_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &minValue, &maxValue, &minIndexPt, &maxIndexPt);
    ValidateStatus(status);
}



Ipp8uC1 Ipp32fC1::scale8u(Ipp32f vMin, Ipp32f vMax) const
{
    Ipp8uC1 dst(width_, height_);

    scale8u(Origin, dst, Origin, MakeSize(width_, height_), vMin, vMax);

    return dst;
}


void Ipp32fC1::scale8u(const IppiPoint& srcPt, Ipp8uC1& dst, const IppiPoint& dstPt, const IppiSize& size, Ipp32f vMin, Ipp32f vMax) const
{
    IppiRect srcRect = MakeRect(srcPt, size);
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiScale_32f8u_C1R(ptr(srcPt), step(), dst.ptr(dstPt), dst.step(), size, vMin, vMax);
    ValidateStatus(status);
}



