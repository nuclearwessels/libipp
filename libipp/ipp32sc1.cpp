
#include "ipp32sc1.h"


using namespace libipp;
using namespace std;



Ipp32sC1::Ipp32sC1(int w, int h) : IppImgBase(w, h)
{
    Ipp32s* buff = ippiMalloc_32s_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_32s_C1 failed");
    buffer_ = unique_ptr<Ipp32s, IppImgDeleter>(buff);
}


Ipp32s* Ipp32sC1::ptr(int x, int y) const
{
    validateXY(x, y);
    //return ((Ipp32s*)((Ipp8u*)buffer_.get() + y * step_)) + x;
    return IppImgBase::ptr(x, y, 1);
}

