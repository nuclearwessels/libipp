

#include "ipp16sc1.h"

using namespace libipp;
using namespace std;


Ipp16sC1::Ipp16sC1(int w, int h) : IppImgBase(w, h)
{
    Ipp16s* buff = ippiMalloc_16s_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_16s_C1 failed");
    buffer_ = unique_ptr<Ipp16s, IppImgDeleter>(buff);
}


Ipp16s* Ipp16sC1::ptr(int x, int y) const
{
    validateXY(x, y);
    //return ((Ipp16s*)((Ipp8u*)buffer_.get() + y * step_)) + x;
    return IppImgBase::ptr(x, y, 1);
}