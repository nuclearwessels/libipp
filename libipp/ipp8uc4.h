#pragma once


#include "libippcore.h"

#include "ipp8uc1.h"


namespace libipp
{

class Ipp8uC4 : public IppImgBase<Ipp8u>
{
public:
	Ipp8uC4(int w, int h);
    //Ipp8uC4(const Ipp8uC4&) = delete;
    //Ipp8uC4& operator = (const Ipp8uC4&) = delete;


	Ipp8u* ptr(int x = 0, int y = 0) const;
    Ipp8u* ptr(const IppiPoint& pt) const;

    void copy(const IppiRect& rect, Ipp8uC4& dst, const IppiPoint& dstPt) const;
    Ipp8uC4 copy(const IppiRect& rect) const;

    void copyMask(const IppiRect& srcRect, Ipp8uC4& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const;
    Ipp8uC4 copyMask(const IppiRect& srcRect, const Ipp8uC1& mask, const IppiPoint& maskPt) const;

    void copyP4(const IppiRect& srcRect,
                Ipp8uC1& dst1, const IppiPoint& dst1Pt,
                Ipp8uC1& dst2, const IppiPoint& dst2Pt,
                Ipp8uC1& dst3, const IppiPoint& dst3Pt,
                Ipp8uC1& dst4, const IppiPoint& dst4Pt) const;

    void copyC4(const Ipp8uC1& src1, const IppiPoint& src1Pt,
                const Ipp8uC1& src2, const IppiPoint& src2Pt,
                const Ipp8uC1& src3, const IppiPoint& src3Pt,
                const Ipp8uC1& src4, const IppiPoint& src4Pt,
                const IppiRect& dstRect) const;

    void mirror(const IppiRect & rect, IppiAxis axis);

    //! ippiAnd_8u_C4IR
    void and(const IppiPoint& srcDstPt, const Ipp8uC4& src, const IppiRect& srcRect);

    //! ippiAnd_8u_C4R
    static void and(const Ipp8uC4& src1, const IppiRect& src1Rect, const Ipp8uC4& src2, const IppiPoint& src2Pt, Ipp8uC4& dst, const IppiPoint& dstPt);

    //! ippiAndC_8u_C4R
    static void andC(const Ipp8uC4& src, const IppiRect& srcRect, Ipp8uC4& dst, const IppiPoint& dstPt, Ipp8u value1, Ipp8u value2, Ipp8u value3, Ipp8u value4);

    //! ippiAndC_8u_C4IR
    void andC(Ipp8u value1, Ipp8u value2, Ipp8u value3, Ipp8u value4, const IppiRect& srcDstRect);


    Ipp8uC4 remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation = IPPI_INTER_NN) const;
    void remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC4& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN) const;
    static void remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC4& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN);


private:

};


}
