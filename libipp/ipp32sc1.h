
#pragma once

#include "libippcore.h"


namespace libipp
{

class Ipp32sC1 : public IppImgBase<Ipp32s>
{
public:
    Ipp32sC1(int w, int h);

    Ipp32s* ptr(int x = 0, int y = 0) const;
};


}