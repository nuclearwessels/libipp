
#include "ipp8uc4.h"


using namespace libipp;
using namespace std;


Ipp8uC4::Ipp8uC4(int w, int h) : IppImgBase(w, h)
{
	Ipp8u* buff = ippiMalloc_8u_C4(w, h, &step_);
	if (buff == 0)
		throw IppException("ippiMalloc_8u_C4 failed");
	buffer_ = unique_ptr<Ipp8u, IppImgDeleter>(buff);
}


Ipp8u* Ipp8uC4::ptr(int x, int y) const
{
	validateXY(x, y);
	return IppImgBase::ptr(x, y, 4);
}


Ipp8u* Ipp8uC4::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


void Ipp8uC4::copy(const IppiRect& rect, Ipp8uC4& dst, const IppiPoint& dstPt) const
{
    validateROI(rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_8u_C4R(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect));
    ValidateStatus(status);
}


Ipp8uC4 Ipp8uC4::copy(const IppiRect& rect) const
{
    validateROI(rect);

    Ipp8uC4 dst(rect.width, rect.height);

    IppStatus status = ippiCopy_8u_C4R(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), MakeSize(rect));
    ValidateStatus(status);

    return dst;
}


void Ipp8uC4::copyMask(const IppiRect& srcRect, Ipp8uC4& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(srcRect));
    mask.validateROI(maskRect);

    IppStatus status = ippiCopy_8u_C4MR(ptr(srcRect.x, srcRect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(srcRect),
        mask.ptr(maskPt), mask.step());
    ValidateStatus(status);
}


Ipp8uC4 Ipp8uC4::copyMask(const IppiRect& srcRect, const Ipp8uC1& mask, const IppiPoint& maskPt) const
{
    validateROI(srcRect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(srcRect));
    mask.validateROI(maskRect);

    Ipp8uC4 dst(srcRect.x, srcRect.y);

    IppStatus status = ippiCopy_8u_C4MR(ptr(srcRect.x, srcRect.y), step(), dst.ptr(), dst.step(), MakeSize(srcRect),
        mask.ptr(maskPt), mask.step());
    ValidateStatus(status);
    
    return dst;
}


void Ipp8uC4::copyP4(const IppiRect& srcRect,
                     Ipp8uC1& dst1, const IppiPoint& dst1Pt,
                     Ipp8uC1& dst2, const IppiPoint& dst2Pt,
                     Ipp8uC1& dst3, const IppiPoint& dst3Pt,
                     Ipp8uC1& dst4, const IppiPoint& dst4Pt) const
{
    validateROI(srcRect);

    IppiRect dst1Rect = MakeRect(dst1Pt, MakeSize(srcRect));
    dst1.validateROI(dst1Rect);

    IppiRect dst2Rect = MakeRect(dst2Pt, MakeSize(srcRect));
    dst2.validateROI(dst2Rect);

    IppiRect dst3Rect = MakeRect(dst3Pt, MakeSize(srcRect));
    dst3.validateROI(dst3Rect);

    IppiRect dst4Rect = MakeRect(dst4Pt, MakeSize(srcRect));
    dst4.validateROI(dst4Rect);

    Ipp8u* dst[4] = { dst1.ptr(dst1Pt), dst2.ptr(dst2Pt), dst3.ptr(dst3Pt), dst4.ptr(dst4Pt) };

    // TODO Do all of the destinations need to be the same size?
    IppStatus status = ippiCopy_8u_C4P4R(ptr(srcRect.x, srcRect.y), step(), dst, dst1.step(), MakeSize(srcRect));
    ValidateStatus(status);
}



void Ipp8uC4::copyC4(const Ipp8uC1& src1, const IppiPoint& src1Pt,
                     const Ipp8uC1& src2, const IppiPoint& src2Pt,
                     const Ipp8uC1& src3, const IppiPoint& src3Pt,
                     const Ipp8uC1& src4, const IppiPoint& src4Pt,
                     const IppiRect& dstRect) const
{
    validateROI(dstRect);

    IppiRect src1Rect = MakeRect(src1Pt, MakeSize(dstRect));
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(dstRect));
    src2.validateROI(src1Rect);

    IppiRect src3Rect = MakeRect(src3Pt, MakeSize(dstRect));
    src3.validateROI(src1Rect);

    IppiRect src4Rect = MakeRect(src4Pt, MakeSize(dstRect));
    src4.validateROI(src1Rect);

    Ipp8u* src[4] = { src1.ptr(src1Pt), src2.ptr(src2Pt), src3.ptr(src3Pt), src4.ptr(src4Pt) };

    // TODO Do these all need to be the same size?

    IppStatus status = ippiCopy_8u_P4C4R(src, src1.step(), ptr(dstRect.x, dstRect.y), step(), MakeSize(dstRect));
    ValidateStatus(status);
}




void Ipp8uC4::mirror(const IppiRect & rect, IppiAxis axis)
{
    validateROI(rect);

    IppStatus status = ippiMirror_8u_C4IR(ptr(rect.x, rect.y), step(), MakeSize(rect), axis);
    ValidateStatus(status);
}


//! ippiAnd_8u_C4IR
void Ipp8uC4::and(const IppiPoint& srcDstPt, const Ipp8uC4& src, const IppiRect& srcRect)
{
    src.validateROI(srcRect);

    IppiRect dstRect = MakeRect(srcDstPt, MakeSize(srcRect));

    IppStatus status = ippiAnd_8u_C4IR(src.ptr(srcRect.x, srcRect.y), src.step(), ptr(srcDstPt.x, srcDstPt.y), step(), MakeSize(srcRect));
    ValidateStatus(status);
}


//! ippiAnd_8u_C4R
void Ipp8uC4::and(const Ipp8uC4& src1, const IppiRect& src1Rect, const Ipp8uC4& src2, const IppiPoint& src2Pt, Ipp8uC4& dst, const IppiPoint& dstPt)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiAnd_8u_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(),
        src2.ptr(src2Pt.x, src2Pt.y), src2.step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(src1Rect));
    ValidateStatus(status);
}




// ippiAndC_8u_C4R
void Ipp8uC4::andC(const Ipp8uC4& src, const IppiRect& srcRect, Ipp8uC4& dst, const IppiPoint& dstPt, Ipp8u value1, Ipp8u value2, Ipp8u value3, Ipp8u value4)
{
    src.validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    const Ipp8u values[4] = { value1, value2, value3, value4 };

    IppStatus status = ippiAndC_8u_C4R(src.ptr(srcRect.x, srcRect.y), src.step(), values,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


//! ippiAndC_8u_C4IR
void Ipp8uC4::andC(Ipp8u value1, Ipp8u value2, Ipp8u value3, Ipp8u value4, const IppiRect& srcRect)
{
    validateROI(srcRect);

    const Ipp8u values[4] = { value1, value2, value3, value4 };

    IppStatus status = ippiAndC_8u_C4IR(values, ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect));
    ValidateStatus(status);
}




Ipp8uC4 Ipp8uC4::remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation) const
{
    Ipp8uC4 dst(width_, height_);
    remap(Origin, pxMap, Origin, pyMap, Origin, dst, Origin, size(), interpolation);
    return dst;
}


void Ipp8uC4::remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC4& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation) const
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C4R(ptr(), size, step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}


void Ipp8uC4::remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC4& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation)
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    src.validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C4R(src.ptr(), size, src.step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}

