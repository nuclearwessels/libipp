
#include "libippcore.h"

#include <map>


using namespace libipp;
using namespace std;



void IppImgDeleter::operator()(void* ippiBuff)
{
    ippiFree(ippiBuff);
}



ValueFormatter::ValueFormatter(int fieldWidth) : fieldWidth_(fieldWidth)
{

}


string ValueFormatter::operator()(Ipp8u value)
{
    stringstream ss;
    ss << std::setw(fieldWidth_) << std::setiosflags(std::ios::right) << (unsigned int)value;
    return ss.str();
}



IppException::IppException() : msg_(nullptr)
{

}


IppException::IppException(const char* msg) : msg_(msg)
{

}


const char* IppException::what() const
{
    return msg_;
}



const map<int, string> CpuFeatures =
{
    std::pair<int, string>(0x00000001, "MMXTM technology"),
    std::pair<int, string>(0x00000002, "Intel� Streaming SIMD Extensions"),
    std::pair<int, string>(0x00000004, "Intel� Streaming SIMD Extensions 2"),
    std::pair<int, string>(0x00000008, "Intel� Streaming SIMD Extensions 3"),
    std::pair<int, string>(0x00000010, "Supplemental Intel� Streaming SIMD Extensions"),
    std::pair<int, string>(0x00000020, "MOVBE instruction"),
    std::pair<int, string>(0x00000040, "Intel� Streaming SIMD Extensions 4.1"),
    std::pair<int, string>(0x00000080, "Intel� Streaming SIMD Extensions 4.2"),
    std::pair<int, string>(0x00000100, "The processor supports Intel� Advanced Vector Extensions(Intel� AVX) instruction set 8"),
    std::pair<int, string>(0x00000200, "The operating system supports Intel� AVX 9"),
    std::pair<int, string>(0x00000400, "Advanced Encryption Standard(AES)"),
    std::pair<int, string>(0x00000800, "PCLMULQDQ instruction"),
    std::pair<int, string>(0x00002000, "Read Random Number instructions"),
    std::pair<int, string>(0x00004000, "16 - bit floating point conversion instructions"),
    std::pair<int, string>(0x00008000, "Intel� Advanced Vector Extensions 2 (Intel� AVX2) instruction set"),
    std::pair<int, string>(0x00010000, "ADCX and ADOX instructions"),
    std::pair<int, string>(0x00020000, "Read Random SEED instruction"),
    std::pair<int, string>(0x00040000, "PREFETCHW instruction"),
    std::pair<int, string>(0x00080000, "Intel� Secure Hash Algorithm Extensions(Intel� SHA Extensions)"),
    std::pair<int, string>(0x00100000, "Intel� Advanced Vector Extensions 512 (Intel� AVX - 512) foundation instructions"),
    std::pair<int, string>(0x00200000, "Intel� AVX - 512 conflict detection instructions"),
    std::pair<int, string>(0x00400000, "Intel� AVX - 512 exponential and reciprocal instructions"),
    std::pair<int, string>(0x80000000, "Intel� Xeon Phi�")
};




void libipp::GetCpuFeatures(vector<string>& features)
{
    features.clear();

    Ipp64u cpuFeatures = ippCPUID_GETINFO_A;
    Ipp32u cpuIdInfoRegs[4] = { 0, 0, 0, 0 };

    IppStatus status = ippGetCpuFeatures(&cpuFeatures, cpuIdInfoRegs);
    ValidateStatus(status);

    for (const auto& i : CpuFeatures)
    {
        if (cpuFeatures & (Ipp64u)i.first)
        {
            features.push_back(i.second);
        }
    }
}



Ipp64u libipp::GetCpuFeatures()
{
    Ipp64u cpuFeatures = ippCPUID_GETINFO_A;
    Ipp32u cpuIdInfoRegs[4] = { 0, 0, 0, 0 };

    IppStatus status = ippGetCpuFeatures(&cpuFeatures, cpuIdInfoRegs);
    ValidateStatus(status);

    return cpuFeatures;
}



string libipp::GetCpuInfo()
{
    stringstream ss;

    // get the library version
    const IppLibraryVersion* libVersion = ippGetLibVersion();
    ss << "Library version: " << libVersion->major << "." << libVersion->minor << "." << libVersion->build << endl;

    int frequency = 0;
    ippGetCpuFreqMhz(&frequency);
    
    int numThreads = 0;
    ippGetNumThreads(&numThreads);

    ss << "CPU: " << frequency << " MHz, " << numThreads << " threads, features: ";

    vector<string> features;
    GetCpuFeatures(features);

    int counter = 0;
    for (const auto& i : features)
    {
        ss << i;
        counter++;
        if (counter < features.size())
        {
            ss << ", ";
        }
    }

    return ss.str();
}




