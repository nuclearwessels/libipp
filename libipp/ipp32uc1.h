
#pragma once

#include "libippcore.h"

#include "ipp32fc1.h"


namespace libipp
{


class Ipp32uC1 : public IppImgBase<Ipp32u>
{
public:
    Ipp32uC1(int w, int h);

    Ipp32u* ptr(int x = 0, int y = 0) const;
    Ipp32u* ptr(const IppiPoint& pt) const;

    void set(Ipp32u value);
    void set(Ipp32u value, const IppiRect& roi);

    void mirror(IppiAxis axis);
    void mirror(const IppiRect& roi, IppiAxis axis);
    static void mirror(const Ipp8uC1& src, const IppiRect& srcROI, Ipp32uC1& dst, const IppiPoint& dstPt, IppiAxis axis);

    static void convert32fC1(const Ipp32uC1& src, const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt);
    void convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const;
    Ipp32fC1 convert32fC1(const IppiRect& srcRect) const;

};

}