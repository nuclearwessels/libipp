
#pragma once


#include "ipp.h"
#include <exception>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <functional>


namespace libipp
{


struct IppImgDeleter
{
    void operator()(void* ippiBuff);
};


struct ValueFormatter
{
    ValueFormatter(int fieldWidth);

    std::string operator() (Ipp8u value);

private: 
    int fieldWidth_;
};


class IppException : public std::exception
{
public:
    IppException();
    IppException(const char* msg);
    const char* what() const;

private:
    const char* msg_;
};

inline IppiSize MakeSize(int w, int h)
{
    IppiSize size;
    size.width = w;
    size.height = h;
    return size;
}

inline IppiSize MakeSize(const IppiRect& rect)
{
    IppiSize size;
    size.width = rect.width;
    size.height = rect.height;
    return size;
}


inline IppiRect MakeRect(int x, int y, int w, int h)
{
    IppiRect rect;
    rect.x = x;
    rect.y = y;
    rect.width = w;
    rect.height = h;
    return rect;
}


inline IppiRect MakeRect(const IppiPoint& pt, const IppiSize& size)
{
    IppiRect rect;
    rect.x = pt.x;
    rect.y = pt.y;
    rect.width = size.width;
    rect.height = size.height;
    return rect;
}


inline IppiPoint MakePoint(int x, int y)
{
    IppiPoint pt;
    pt.x = x;
    pt.y = y;
    return pt;
}


inline IppiPoint MakePoint(const IppiRect& rect)
{
    IppiPoint pt;
    pt.x = rect.x;
    pt.y = rect.y;
    return pt;
}


inline IppiPoint_32f MakePoint(float x, float y)
{
    IppiPoint_32f pt;
    pt.x = x;
    pt.y = y;
    return pt;
}


constexpr IppiPoint Origin = { 0, 0 };


inline void ValidateStatus(IppStatus status)
{
    if (status != ippStsNoErr)
    {
        std::stringstream ss;
        ss << "IppStatus = " << (int)status << ": " << ippGetStatusString(status);
        throw IppException(ss.str().c_str());
    }
}

    
inline bool AreSameSize(const IppiSize& r1, const IppiSize& r2)
{
    return (r1.width != r2.width || r1.height != r2.height) ? false : true;
}


inline bool AreSameSize(const IppiRect& r1, const IppiRect& r2)
{
    return (r1.width != r2.width || r1.height != r2.height) ? false : true;
}


inline void ValidateSameSize(const IppiRect& r1, const IppiRect& r2)
{
    if (AreSameSize(r1, r2))
        throw IppException("Rectangle not same size");
}


inline void Validate(int expr)
{
    if (!expr)
    {
        throw IppException("Validate failed");
    }
}


template<class T>
class IppImgBase
{
public:
    IppImgBase(int w, int h);
    IppImgBase(IppImgBase<T>& other) = default;
    IppImgBase(IppImgBase<T>&& other) = default;
    IppImgBase<T>& operator = (const IppImgBase<T>& other) = default;
    IppImgBase<T>& operator = (IppImgBase<T>&& other) = default;

    virtual ~IppImgBase();

    IppiSize size() const;
    int width() const;
    int height() const;
    int step() const;
    IppiRect rect() const;

    virtual T* ptr(int x = 0, int y = 0) const = 0;
    virtual T* ptr(const IppiPoint& pt) const = 0;

    void init(const std::initializer_list<T>& initValues);

    void validateXY(int x, int y) const;
    void validateROI(const IppiRect& rect) const;
    
    std::string str(int fieldWidth = 3) const;

protected:
    std::unique_ptr<T, IppImgDeleter> buffer_;
    int width_, height_, step_;

    T* ptr(int x, int y, int planes) const;    
};


template<class T>
IppImgBase<T>::IppImgBase(int w, int h) : width_(w), height_(h)
{
    Validate(w > 0);
    Validate(h > 0);
}


template <class T>
IppImgBase<T>::~IppImgBase()
{
}


template<class T>
void IppImgBase<T>::init(const std::initializer_list<T>& initValues)
{
    if (initValues.size() != width_ * height_)
    {
        std::stringstream ss;
        ss << "Invalid number of values: Expected " << width_ * height_ << ", received " << initValues.size();
        throw IppException(ss.str().c_str());
    }
        
    auto i = initValues.begin();
    for (int y = 0; y < height_; y++)
    {
        T* p = ptr(0, y);
        for (int x = 0; x < width_; x++, p++, i++)
        {
            *p = *i;
        }
    }
}



template<class T>
inline T* IppImgBase<T>::ptr(int x, int y, int planes) const
{
    return ((T*)((Ipp8u*)buffer_.get() + y * step_)) + x * planes;
}


template<class T>
inline T* IppImgBase<T>::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y, 1);
}


template<class T>
int IppImgBase<T>::step() const
{
    return step_;
}


template<class T>
inline int IppImgBase<T>::width() const
{
    return width_;
}


template<class T>
inline int IppImgBase<T>::height() const
{
    return height_;
}



template<class T>
inline IppiSize IppImgBase<T>::size() const
{
    return MakeSize(width_, height_);
}


template<class T>
inline IppiRect IppImgBase<T>::rect() const
{
    return MakeRect(0, 0, width_, height_);
}

template<class T>
inline void IppImgBase<T>::validateXY(int x, int y) const
{
    if (x >= width_)
        throw IppException("Exceeds width");

    if (y >= height_)
        throw IppException("Exceeds height");
}


template<class T>
inline void IppImgBase<T>::validateROI(const IppiRect& roi) const
{
    if (roi.x + roi.width > width_)
        throw IppException("ROI exceeds width");

    if (roi.y + roi.height > height_)
        throw IppException("ROI exceeds height");
}


template<class T>
std::string IppImgBase<T>::str(int fieldWidth) const
{
    std::stringstream ss;

    // print out the column headers
    for (int i = 0; i < fieldWidth + 1; i++)
        ss << " ";

    for (int x = 0; x < width_; x++)
        ss << std::setw(fieldWidth) << std::setiosflags(std::ios::right) << x << " ";

    ss << endl;

    ValueFormatter formatter(fieldWidth);

    for (int y = 0; y < height_; y++)
    {
        T* p = ptr(0, y);
        ss << std::setw(fieldWidth) << std::setiosflags(std::ios::right) << y << " ";

        for (int x = 0; x < width_; x++, p++)
        {
            T value = *p;
            ss << formatter(value) << " ";
        }
        
        ss << endl;
    }

    return ss.str();
}



template <class ImageType, class PrimitiveType>
bool CompareImageEqualValue(const ImageType& img, PrimitiveType value)
{
    const PrimitiveType True = (PrimitiveType)-1;

    Ipp8uC1 dstCompare(img.width(), img.height());

    img.compareC(img.rect(), value, dstCompare, Origin, ippCmpEq);

    Ipp8u minValue;
    dstCompare.min(dstCompare.rect(), minValue);

    return minValue == True;
}



template <class ImageType, class PrimitiveType>
bool CompareImagesEqual(const ImageType& src1, const ImageType& src2)
{
    if (!AreSameSize(src1.size(), src2.size()))
        return false;

    const PrimitiveType True = (PrimitiveType)-1;

    Ipp8uC1 dstCompare(src1.width(), src1.height());
    src1.compare(src1.rect(), src2, Origin, dstCompare, Origin, ippCmpEq);

    Ipp8u minValue;
    dstCompare.min(dstCompare.rect(), minValue);
    
    return minValue == True;
}



void GetCpuFeatures(std::vector<std::string>& features);

Ipp64u GetCpuFeatures();

std::string GetCpuInfo();

class Ipp8uC1;
class Ipp16uC1;
class Ipp16sC1;
class Ipp32sC1;
class Ipp32scC1;
class Ipp32fC1;


}