
#pragma once

#include "libippcore.h"

#include "ipp32fc1.h"


namespace libipp
{


class Ipp16uC1 : public IppImgBase<Ipp16u>
{
public:
    Ipp16uC1(int w, int h);

    Ipp16u* ptr(int x = 0, int y = 0) const;
    Ipp16u* ptr(const IppiPoint& pt) const;

    void copy(const IppiRect& rect, Ipp16uC1& dst, const IppiPoint& dstPt) const;
    Ipp16uC1 copy(const IppiRect& rect) const;

    void set(Ipp16u value);
    void set(Ipp16u value, const IppiRect& roi);

    void setMask(Ipp16u value, const Ipp8uC1& mask);
    void setMask(Ipp16u value, const IppiRect& dstRect, const Ipp8uC1& mask, const IppiPoint& maskPt);

    void mirror(IppiAxis axis);
    void mirror(const IppiRect& roi, IppiAxis axis);
    static void mirror(const Ipp8uC1& src, const IppiRect& srcROI, Ipp8uC1& dst, const IppiPoint& dstPt, IppiAxis axis);

    static void convert32fC1(const Ipp16uC1& src, const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt);
    void convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const;
    Ipp32fC1 convert32fC1(const IppiRect& srcRect) const;

    void remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp16uC1& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN);

    static void remap(const Ipp16uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp16uC1& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN);

};

}