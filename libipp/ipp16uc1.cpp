
#include "ipp16uc1.h"

using namespace libipp;
using namespace std;


Ipp16uC1::Ipp16uC1(int w, int h) : IppImgBase(w, h)
{
    Ipp16u* buff = ippiMalloc_16u_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_16u_C1 failed");
    buffer_ = unique_ptr<Ipp16u, IppImgDeleter>(buff);
}


Ipp16u* Ipp16uC1::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 1);
}


Ipp16u* Ipp16uC1::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}



void Ipp16uC1::set(Ipp16u value)
{
    IppiRect roi = MakeRect(0, 0, width_, height_);
    set(value, roi);
}


void Ipp16uC1::set(Ipp16u value, const IppiRect& roi)
{
    validateROI(roi);

    IppStatus status = ippiSet_16u_C1R(value, ptr(roi.x, roi.y), step_, MakeSize(roi));
    ValidateStatus(status);
}


void Ipp16uC1::setMask(Ipp16u value, const Ipp8uC1& mask)
{
    setMask(value, rect(), mask, Origin);
}


void Ipp16uC1::setMask(Ipp16u value, const IppiRect& dstRect, const Ipp8uC1& mask, const IppiPoint& maskPt)
{
    validateROI(dstRect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(dstRect));
    mask.validateROI(maskRect);

    IppStatus status = ippiSet_16u_C1MR(value, ptr(dstRect.x, dstRect.y), step(), MakeSize(dstRect), mask.ptr(maskPt), mask.step());
    ValidateStatus(status);
}



void Ipp16uC1::copy(const IppiRect& rect, Ipp16uC1& dst, const IppiPoint& dstPt) const
{
    validateROI(rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_16u_C1R(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect));
    ValidateStatus(status);
}


Ipp16uC1 Ipp16uC1::copy(const IppiRect& rect) const
{
    validateROI(rect);

    Ipp16uC1 dst(rect.width, rect.height);

    IppStatus status = ippiCopy_16u_C1R(ptr(rect.x, rect.y), step(), dst.ptr(), dst.step(), MakeSize(rect));
    ValidateStatus(status);

    return dst;
}



void Ipp16uC1::convert32fC1(const Ipp16uC1& src, const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt)
{
    src.validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_16u32f_C1R(src.ptr(srcRect.x, srcRect.y), src.step(), 
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect) );
    ValidateStatus(status);
}


void Ipp16uC1::convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_16u32f_C1R(ptr(srcRect.x, srcRect.y), step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


Ipp32fC1 Ipp16uC1::convert32fC1(const IppiRect& srcRect) const
{
    validateROI(srcRect);

    Ipp32fC1 dst(srcRect.width, srcRect.height);
    
    IppStatus status = ippiConvert_16u32f_C1R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);

    return dst;
}



void Ipp16uC1::remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp16uC1& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation)
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_16u_C1R(ptr(), size, step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}


void Ipp16uC1::remap(const Ipp16uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp16uC1& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation)
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    src.validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_16u_C1R(src.ptr(), size, src.step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}

