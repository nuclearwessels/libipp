

#pragma once

#include "libippcore.h"

#include "ipp32fc1.h"


namespace libipp
{

class Ipp8uC3 : public IppImgBase<Ipp8u>
{
public:
    Ipp8uC3(int w, int h);

    Ipp8u* ptr(int x = 0, int y = 0) const;
    Ipp8u* ptr(const IppiPoint& pt) const;

    void copy(const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt) const;
    Ipp8uC3 copy(const IppiRect& srcRect) const;

    static void copyP3(const Ipp8uC1& src1, const IppiRect& src1Rect,
                       const Ipp8uC1& src2, const IppiPoint& src2Pt,
                       const Ipp8uC1& src3, const IppiPoint& src3Pt,
                       Ipp8uC3& dst, const IppiPoint& dstPt);

    //! Adds a constant to the specified ROI.
    void addC(Ipp8u value1, Ipp8u value2, Ipp8u value3, const IppiRect& rect, int scaleFactor = 0);
    void addC(Ipp8u value1, Ipp8u value2, Ipp8u value3, const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt, int scaleFactor = 0);

    //! Add a second image to this image.
    void add(const IppiRect& srcDstRect, const Ipp8uC3& src2, const IppiPoint& src2Pt, int scaleFactor = 0);

    //! Add two images to a third
    static void add(const Ipp8uC3& src1, const IppiRect& src1Rect, const Ipp8uC3& src2, const IppiPoint& src2Pt, Ipp8uC3& dst, const IppiPoint& dstPt, int scaleFactor = 0);


    void subC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);
    void subC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);

    void mulC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);
    void mulC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);

    void divC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);
    void divC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor = 0);


    void countInRange(const IppiRect& rect, int count[3], Ipp8u lowerBound[3], Ipp8u upperBound[3]) const;


    void mirror(IppiAxis axis);
    void mirror(const IppiRect& rect, IppiAxis axis);

    
    Ipp8uC3 remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation = IPPI_INTER_NN) const;
    void remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC3& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN) const;
    static void remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
        const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC3& dst, const IppiPoint& dstPt, IppiSize size,
        int interpolation = IPPI_INTER_NN);


private:

};


}