#pragma once

#include <memory>
#include <sstream>

namespace libipp
{

struct IppSignalDeleter
{
    void operator()(void* ippsBuff);
};

template<class T>
class IppSignalBase
{
public:
	int size() const;

	void validateOffsetLength(int offset, int length);

	T* ptr(int x = 0) const;
	T* operator[] (int x) const;

protected:
	std::unique_ptr<T, IppSignalDeleter> buffer_;
	int size_;
};

template <class T>
int IppSignalBase<T>::size() const
{
	return size_;
}


template <class T>
void IppSignalBase<T>::validateOffsetLength(int offset, int length)
{
	if (offset + length > size_)
	{
		std::stringstream ss;
		ss << "validateOffsetLength failed(offset=" << offset << ", length=" << length << ", size=" << size_ << ")";
		throw IppException(ss.str().c_str());
	}
}

template<class T>
inline T * IppSignalBase<T>::ptr(int x) const
{
	if (x >= size_)
	{
		std::stringstream ss;
		ss << "boundary check failed: " << x << " >= " << size_;
		throw IppException(ss.str().c_str());
	}
	return buffer_.get() + x;
}

template<class T>
inline T * IppSignalBase<T>::operator[](int x) const
{
	return ptr(x);
}


class Ipps8s : public IppSignalBase<Ipp8s>
{
public:
	Ipps8s(int size);
	
};


class Ipps8u : public IppSignalBase<Ipp8u>
{
public:
	Ipps8u(int size);

	void set(Ipp8u value, int offset, int length);
	void copy(int srcOffset, Ipps8u& dst, int dstOffset, int length);
};

}