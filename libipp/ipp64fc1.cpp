
#include "ipp64fc1.h"

#include <functional>
#include <intrin.h>

using namespace libipp;
using namespace std;




typedef std::function<bool(double, double)> CompareOperation;


CompareOperation LessEqualOp = [](double a, double b)
{
    return a <= b;
};


CompareOperation LessOp = [](double a, double b)
{
    return a < b;
};


CompareOperation EqualOp = [](double a, double b)
{
    return a == b;
};


CompareOperation GreaterOp = [](double a, double b)
{
    return a > b;
};


CompareOperation GreaterEqualOp = [](double a, double b)
{
    return a >= b;
};


Ipp64fC1::Ipp64fC1(int w, int h) : IppImgBase(w, h)
{
    Ipp64f* buff = (Ipp64f*) ippiMalloc_32fc_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_32fc_C1 failed");

    buffer_ = unique_ptr<Ipp64f, IppImgDeleter>(buff);
}



Ipp64f* Ipp64fC1::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 1);
}


Ipp64f* Ipp64fC1::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


bool Ipp64fC1::operator == (Ipp64f value) const
{
    return CompareImageEqualValue<Ipp64fC1, Ipp64f>(*this, value);
}


bool Ipp64fC1::operator == (const Ipp64fC1& src2) const
{
    return CompareImagesEqual<Ipp64fC1, Ipp64f>(*this, src2);
}


void Ipp64fC1::compare(const IppiRect& srcRect, const Ipp64fC1& src2, const IppiPoint& src2Pt,
    Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    // TODO Reimplement using SIMD instructions.

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(srcRect));
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    CompareOperation op;

    switch (cmpOp)
    {
    case ippCmpLess:
        op = LessOp;
        break;

    case ippCmpLessEq:
        op = LessEqualOp;
        break;

    case ippCmpEq:
        op = EqualOp;
        break;

    case ippCmpGreaterEq:
        op = GreaterEqualOp;
        break;

    case ippCmpGreater:
        op = GreaterOp;
        break;

    default:
        throw runtime_error("Invalid ippCmpOp");
    }

    Ipp64f* psrc1, *psrc2;
    Ipp8u* pdst;

    for (int r = 0; r < srcRect.height; r++)
    {
        psrc1 = ptr(srcRect.x, r + srcRect.y);
        psrc2 = src2.ptr(src2Pt.x, r + src2Pt.y);
        pdst = dst.ptr(dstPt.x, r + dstPt.y);

        for (int c = 0; c < srcRect.width; c++)
        {
            *pdst = op(*psrc1, *psrc2) ? 0xFF : 0x00;
            psrc1++;
            psrc2++;
            pdst++;
        }
    }

}



void Ipp64fC1::compareC(const IppiRect& srcRect, Ipp64f value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    // TODO Reimplement using SIMD instructions.

    CompareOperation op;

    switch (cmpOp)
    {
    case ippCmpLess:
        op = LessOp;
        break;

    case ippCmpLessEq:
        op = LessEqualOp;
        break;

    case ippCmpEq:
        op = EqualOp;
        break;

    case ippCmpGreaterEq:
        op = GreaterEqualOp;
        break;

    case ippCmpGreater:
        op = GreaterOp;
        break;

    default:
        throw runtime_error("Invalid cmpOp");
    }

    Ipp64f* psrc;
    Ipp8u* pdst;

    for (int r = 0; r < srcRect.height; r++)
    {
        psrc = ptr(srcRect.x, r + srcRect.y);
        pdst = dst.ptr(dstPt.x, r + dstPt.y);

        for (int c = 0; c < srcRect.width; c++)
        {
            *pdst = op(*psrc, value) ? 0xFF : 0x00;
            psrc++;
            pdst++;
        }
    }

}

