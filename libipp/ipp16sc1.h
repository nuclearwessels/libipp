#pragma once

#include "libipp.h"


namespace libipp
{

class Ipp16sC1 : public IppImgBase<Ipp16s>
{
public:
    Ipp16sC1(int w, int h);

    Ipp16s* ptr(int x = 0, int y = 0) const;

private:

};

}