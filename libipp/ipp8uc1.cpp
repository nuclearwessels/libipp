

#include "ipp8uc1.h"

#include "ipp16uc1.h"
#include "ipp16sc1.h"
#include "ipp32sc1.h"
#include "ipp32fc1.h"

#include <algorithm>

using namespace libipp;
using namespace std;



Ipp8uC1::Ipp8uC1(int w, int h) : IppImgBase(w, h)
{
    Ipp8u* buff = ippiMalloc_8u_C1(w, h, &step_);
    if (buff == nullptr)
        throw IppException("ippiMalloc_8u_C1 failed");

    buffer_ = unique_ptr<Ipp8u, IppImgDeleter>(buff);
}



bool Ipp8uC1::operator == (Ipp8u value) const
{
    return CompareImageEqualValue<Ipp8uC1, Ipp8u>(*this, value);
}


bool Ipp8uC1::operator == (const Ipp8uC1& src2) const
{
    return CompareImagesEqual<Ipp8uC1, Ipp8u>(*this, src2);
}





Ipp8u* Ipp8uC1::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 1);
}


Ipp8u* Ipp8uC1::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


//IPPAPI(IppStatus, ippiCopy_8u_P3C3R, (const  Ipp8u* const pSrc[3],
//    int srcStep, Ipp8u* pDst, int dstStep, IppiSize roiSize))
void Ipp8uC3::copyP3(const Ipp8uC1& src1, const IppiRect& src1Rect,
                     const Ipp8uC1& src2, const IppiPoint& src2Pt,
                     const Ipp8uC1& src3, const IppiPoint& src3Pt,
                     Ipp8uC3& dst, const IppiPoint& dstPt)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
    src2.validateROI(src2Rect);

    IppiRect src3Rect = MakeRect(src3Pt, MakeSize(src1Rect));
    src3.validateROI(src3Rect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    Ipp8u* src[3] = { src1.ptr(src1Rect.x, src1Rect.y),
                      src2.ptr(src2Pt),
                      src3.ptr(src3Pt) };
    IppStatus status = ippiCopy_8u_P3C3R(src, src1.step(), dst.ptr(dstPt), dst.step(), MakeSize(src1Rect));
    ValidateStatus(status);
}




void Ipp8uC1::set(Ipp8u value, const IppiRect& rect)
{
    validateROI(rect);
    IppStatus status = ippiSet_8u_C1R(value, ptr(rect.x, rect.y), step_, MakeSize(rect));
    ValidateStatus(status);
}


void Ipp8uC1::setMask(Ipp8u value, const IppiRect& roi, const Ipp8uC1& mask, const IppiPoint& maskPt)
{
    validateROI(roi);

    IppiRect maskROI = MakeRect(maskPt, MakeSize(roi));
    mask.validateROI(maskROI);

    IppStatus status = ippiSet_8u_C1MR(value, ptr(roi.x, roi.y), step_, MakeSize(roi), mask.ptr(maskPt.x, maskPt.y), mask.step());
    ValidateStatus(status);
}


void Ipp8uC1::mirror(const IppiRect& roi, IppiAxis axis)
{
    validateROI(roi);
    IppStatus status = ippiMirror_8u_C1IR(ptr(roi.x, roi.y), step_, MakeSize(roi), axis);
    ValidateStatus(status);
}


void Ipp8uC1::mirror(const IppiRect& srcROI, Ipp8uC1& dst, const IppiPoint& dstPt, IppiAxis axis) const
{
    validateROI(srcROI);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcROI));
    dst.validateROI(dstROI);

    IppStatus status = ippiMirror_8u_C1R(ptr(srcROI.x, srcROI.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcROI), axis);
    ValidateStatus(status);
}


void Ipp8uC1::convert16uC1(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiConvert_8u16u_C1R(ptr(srcRect.x, srcRect.y), step_,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}



void Ipp8uC1::convert16sC1(const IppiRect& srcRect, Ipp16sC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiConvert_8u16s_C1R(ptr(srcRect.x, srcRect.y), step_,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::convert32sC1(const IppiRect& srcRect, Ipp32sC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiConvert_8u32s_C1R(ptr(srcRect.x, srcRect.y), step_,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiConvert_8u32f_C1R(ptr(srcRect.x, srcRect.y), step_,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


Ipp32fC1 Ipp8uC1::convert32fC1(const IppiRect& srcRect) const
{
    validateROI(srcRect);

    Ipp32fC1 dst(srcRect.width, srcRect.height);
    
    IppStatus status = ippiConvert_8u32f_C1R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);

    return dst;
}


void Ipp8uC1::scale16uC1(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiScale_8u16u_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::scale16sC1(const IppiRect& srcRect, Ipp16sC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiScale_8u16s_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::scale32sC1(const IppiRect& srcRect, Ipp32sC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiScale_8u32s_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::scale32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt, Ipp32f min, Ipp32f max) const
{
    validateROI(srcRect);

    IppiRect dstROI = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstROI);

    IppStatus status = ippiScale_8u32f_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcRect), min, max);
    ValidateStatus(status);

}



void Ipp8uC1::copy(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_8u_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(srcRect));
    ValidateStatus(status);
}


void Ipp8uC1::copyMask(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, const Ipp8uC1& mask, const IppiPoint& maskPt) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);
    
    IppiRect maskRect = MakeRect(maskPt, size);
    mask.validateROI(maskRect);

    IppStatus status = ippiCopy_8u_C1MR(ptr(srcRect.x, srcRect.y), step_, 
                                        dst.ptr(dstPt.x, dstPt.y), dst.step(), size, 
                                        mask.ptr(maskPt.x, maskPt.y), mask.step());
    ValidateStatus(status);
}


void Ipp8uC1::copyC3(const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_8u_C1C3R(ptr(srcRect.x, srcRect.y), step_, 
                                         dst.ptr(dstPt.x, dstPt.y), dst.step(), 
                                         size);
    ValidateStatus(status);
}



void Ipp8uC1::copyConstBorder(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect,
    int topBorderHeight, int bottomBorderHeight, Ipp8u value) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopyConstBorder_8u_C1R(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect),
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(dstRect), topBorderHeight, bottomBorderHeight, value );
    ValidateStatus(status);
}


void Ipp8uC1::copyReplicateBorder(const IppiRect& srcRect, const IppiSize& dstSize, int topBorderHeight, int leftBorderWidth)
{
    validateROI(srcRect);

    Validate(dstSize.width <= srcRect.width);
    Validate(dstSize.height <= srcRect.height);

    IppStatus status = ippiCopyReplicateBorder_8u_C1IR(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect), 
        dstSize, topBorderHeight, leftBorderWidth);
    ValidateStatus(status);
}


void Ipp8uC1::copyReplicateBorder(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect,
    int topBorderHeight, int leftBorderWidth) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopyReplicateBorder_8u_C1R(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect),
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(dstRect), topBorderHeight, leftBorderWidth);
    ValidateStatus(status);
}


void Ipp8uC1::copySubpix(const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopySubpix_8u_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(), 
        size, dx, dy);
    ValidateStatus(status);
}


void Ipp8uC1::copySubpix(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopySubpix_8u32f_C1R(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        size, dx, dy);
    ValidateStatus(status);
}


void Ipp8uC1::copySubpix(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt, Ipp32f dx, Ipp32f dy, int scaleFactor) const
{
    validateROI(srcRect);

    IppiSize size = MakeSize(srcRect);
    IppiRect dstRect = MakeRect(dstPt, size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopySubpix_8u16u_C1R_Sfs(ptr(srcRect.x, srcRect.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(),
        size, dx, dy, scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC1::copySubpixIntersect(const IppiRect& srcRect, Ipp8uC1& dst, const IppiRect& dstRect,
    const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);
    
    IppStatus status = ippiCopySubpixIntersect_8u_C1R(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect), 
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(dstRect), pt, &min, &max);
    ValidateStatus(status);
}

void Ipp8uC1::copySubpixIntersect(const IppiRect& srcRect, Ipp32fC1& dst, const IppiRect& dstRect,
    const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopySubpixIntersect_8u32f_C1R(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect),
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(dstRect), pt, &min, &max);
    ValidateStatus(status);
}

void Ipp8uC1::copySubpixIntersect(const IppiRect& srcRect, Ipp16uC1& dst, const IppiRect& dstRect,
    const IppiPoint_32f& pt, IppiPoint& min, IppiPoint& max, int scaleFactor) const
{
    validateROI(srcRect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCopySubpixIntersect_8u16u_C1R_Sfs(ptr(srcRect.x, srcRect.y), step_, MakeSize(srcRect),
        dst.ptr(dstRect.x, dstRect.y), dst.step(), MakeSize(dstRect), pt, &min, &max, scaleFactor);
    ValidateStatus(status);
}



void Ipp8uC1::dupC3(const IppiPoint& pt, Ipp8uC3& dst, const IppiPoint& dstPt, const IppiSize& size) const
{
	IppiRect srcRect = MakeRect(pt, size);
	IppiRect dstRect = MakeRect(dstPt, size);

	validateROI(srcRect);
	dst.validateROI(dstRect);
	
	IppStatus status = ippiDup_8u_C1C3R(ptr(pt.x, pt.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(), size);
	ValidateStatus(status);
}


void Ipp8uC1::dupC4(const IppiPoint& pt, Ipp8uC4& dst, const IppiPoint& dstPt, const IppiSize& size) const
{
	IppiRect srcRect = MakeRect(pt, size);
	IppiRect dstRect = MakeRect(dstPt, size);

	validateROI(srcRect);
	dst.validateROI(dstRect);

	IppStatus status = ippiDup_8u_C1C4R(ptr(pt.x, pt.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(), size);
	ValidateStatus(status);
}

void Ipp8uC1::transpose(const IppiRect& roi)
{
	validateROI(roi);

	IppStatus status = ippiTranspose_8u_C1IR(ptr(roi.x, roi.y), step_, MakeSize(roi));
	ValidateStatus(status);
}

void Ipp8uC1::transpose(const IppiPoint& srcPt, Ipp8uC1& dst, const IppiPoint& dstPt, const IppiSize& size) const
{
	IppiRect srcROI = MakeRect(srcPt, size);
	validateROI(srcROI);
	
	IppiRect dstROI = MakeRect(dstPt, size);
	dst.validateROI(dstROI);

	IppStatus status = ippiTranspose_8u_C1R(ptr(srcPt.x, srcPt.y), step_, dst.ptr(dstPt.x, dstPt.y), dst.step(), size);
	ValidateStatus(status);
}

#if IPP_VERSION_MAJOR == 7

#else

void Ipp8uC1::addRandUniform(const IppiRect& roi, Ipp8u low, Ipp8u high, unsigned int seed)
{
	validateROI(roi);

	IppStatus status = ippiAddRandUniform_8u_C1IR(ptr(roi.x, roi.y), step_, MakeSize(roi), low, high, &seed);
	ValidateStatus(status);
}

#endif


#if IPP_VERSION_MAJOR == 7

#else

void Ipp8uC1::addRandGauss(const IppiRect& roi, Ipp8u mean, Ipp8u stDev, unsigned int seed)
{
	validateROI(roi);

	IppStatus status = ippiAddRandGauss_8u_C1IR(ptr(roi.x, roi.y), step_, MakeSize(roi), mean, stDev, &seed);
	ValidateStatus(status);
}

#endif


void Ipp8uC1::imageJaehne(const IppiRect& roi)
{
	validateROI(roi);

	IppStatus status = ippiImageJaehne_8u_C1R(ptr(roi.x, roi.y), step_, MakeSize(roi));
	ValidateStatus(status);
}


void Ipp8uC1::imageRamp(const IppiRect& roi, float offset, float slope, IppiAxis axis)
{
	validateROI(roi);

	IppStatus status = ippiImageRamp_8u_C1R(ptr(roi.x, roi.y), step_, MakeSize(roi), offset, slope, axis);
	ValidateStatus(status);
}


void Ipp8uC1::sampleLine(const IppiRect& roi, Ipps8u& dst, int dstOffset, const IppiPoint& pt1, const IppiPoint& pt2) const
{
	int buflen = std::max(abs(pt2.x - pt1.x) + 1, abs(pt2.y - pt1.y) + 1);

	dst.validateOffsetLength(dstOffset, buflen);
	validateROI(roi);

	IppStatus status = ippiSampleLine_8u_C1R( ptr(roi.x, roi.y), step_, MakeSize(roi), 
		dst.ptr(dstOffset), pt1, pt2);
	ValidateStatus(status);
}


void Ipp8uC1::min(const IppiRect& rect, Ipp8u& minValue) const
{
    validateROI(rect);

    IppStatus status = ippiMin_8u_C1R(ptr(rect.x, rect.y), step_, MakeSize(rect), &minValue);
    ValidateStatus(status);
}


void Ipp8uC1::min(const IppiRect& rect, Ipp8u& minValue, int& indexX, int& indexY) const
{
    validateROI(rect);

    IppStatus status = ippiMinIndx_8u_C1R(ptr(rect.x, rect.y), step_, MakeSize(rect), &minValue, &indexX, &indexY);
    ValidateStatus(status);
}


void Ipp8uC1::max(const IppiRect& rect, Ipp8u& maxValue) const
{
    validateROI(rect);

    IppStatus status = ippiMax_8u_C1R(ptr(rect.x, rect.y), step_, MakeSize(rect), &maxValue);
    ValidateStatus(status);
}

void Ipp8uC1::max(const IppiRect& rect, Ipp8u& maxValue, int& indexX, int& indexY) const
{
    validateROI(rect);

    IppStatus status = ippiMaxIndx_8u_C1R(ptr(rect.x, rect.y), step_, MakeSize(rect), &maxValue, &indexX, &indexY);
    ValidateStatus(status);
}



void Ipp8uC1::add(Ipp8u value, const IppiRect& rect, int scaleFactor)
{
    validateROI(rect);

    IppStatus status = ippiAddC_8u_C1IRSfs(value, ptr(rect.x, rect.y), step_, MakeSize(rect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC1::add(Ipp8u value, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, int scaleFactor)
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt.x, dstPt.y, srcRect.width, srcRect.height);
    dst.validateROI(dstRect);

    IppStatus status = ippiAddC_8u_C1RSfs( ptr(srcRect.x, srcRect.y), step_, value, dst.ptr(dstPt.x, dstPt.y),
        dst.step(), MakeSize(srcRect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC1::add(const IppiRect& srcDstRect, const Ipp8uC1& src2, const IppiPoint& src2Pt, int scaleFactor)
{
    validateROI(srcDstRect);

    src2.validateROI(MakeRect(src2Pt.x, src2Pt.y, srcDstRect.width, srcDstRect.height));

    IppStatus status = ippiAdd_8u_C1IRSfs(src2.ptr(src2Pt.x, src2Pt.y), src2.step(), ptr(srcDstRect.x, srcDstRect.y),
        step_, MakeSize(srcDstRect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC1::add(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt, int scaleFactor)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt.x, src2Pt.y, src1Rect.width, src1Rect.height);
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt.x, dstPt.y, src1Rect.width, src1Rect.height);
    dst.validateROI(dstRect);

    IppStatus status = ippiAdd_8u_C1RSfs(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), src2.ptr(src2Pt.x, src2Pt.y),
        src2.step(), dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(src1Rect), scaleFactor);
    ValidateStatus(status);
}




//! ippiAnd_8u_C1IR
void Ipp8uC1::and(const IppiPoint& srcDstPt, const Ipp8uC1& src, const IppiRect& srcRect)
{
	src.validateROI(srcRect);

	IppiRect dstRect = MakeRect(srcDstPt, MakeSize(srcRect));

	IppStatus status = ippiAnd_8u_C1IR(src.ptr(srcRect.x, srcRect.y), src.step(), ptr(srcDstPt.x, srcDstPt.y), step(), MakeSize(srcRect));
	ValidateStatus(status);
}


//! ippiAnd_8u_C1R
void Ipp8uC1::and(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt)
{
	src1.validateROI(src1Rect);

	IppiRect src2Rect = MakeRect(src2Pt, MakeSize(src1Rect));
	src2.validateROI(src2Rect);

	IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
	dst.validateROI(dstRect);

	IppStatus status = ippiAnd_8u_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(),
									  src2.ptr(src2Pt.x, src2Pt.y),     src2.step(),
		                              dst.ptr(dstPt.x, dstPt.y),        dst.step(),
		                              MakeSize(src1Rect));
    ValidateStatus(status);
}


//IPPAPI(IppStatus, ippiAndC_8u_C1R, (const Ipp8u* pSrc, int srcStep, Ipp8u value, Ipp8u* pDst, int dstStep, IppiSize roiSize))
//IPPAPI(IppStatus, ippiAndC_8u_C1IR, (Ipp8u value, Ipp8u* pSrcDst, int srcDstStep, IppiSize roiSize))


// ippiAndC_8u_C1R
void Ipp8uC1::andC(const Ipp8uC1& src, const IppiRect& srcRect, Ipp8uC1& dst, const IppiPoint& dstPt, Ipp8u value)
{
    src.validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiAndC_8u_C1R(src.ptr(srcRect.x, srcRect.y), src.step(), value, 
                                       dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


//! ippiAndC_8u_C1IR
void Ipp8uC1::andC(Ipp8u value, const IppiRect& srcRect)
{
    validateROI(srcRect);

    IppStatus status = ippiAndC_8u_C1IR(value, ptr(srcRect.x, srcRect.y), step(), MakeSize(srcRect));
    ValidateStatus(status);
}



void Ipp8uC1::compare(const Ipp8uC1& src1, const IppiRect& src1Rect, const Ipp8uC1& src2, const IppiPoint& src2Pt,
    Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp)
{
    src1.validateROI(src1Rect);

    IppiSize size = MakeSize(src1Rect);
    IppiRect src2Rect = MakeRect(src2Pt, size);
    IppiRect dstRect = MakeRect(dstPt, size);

    src2.validateROI(src2Rect);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompare_8u_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), src2.ptr(src2Pt.x, src2Pt.y), src2.step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), size, cmpOp);
    ValidateStatus(status);
}


void Ipp8uC1::compare(const IppiRect& srcRect, const Ipp8uC1& src2, const IppiPoint& src2Pt, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    IppiSize src1Size = MakeSize(srcRect);

    IppiRect src2Rect = MakeRect(src2Pt, src1Size);
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt, src1Size);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompare_8u_C1R(ptr(srcRect.x, srcRect.y), step_,
        src2.ptr(src2Pt.x, src2Pt.y), src2.step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), src1Size, cmpOp);
    ValidateStatus(status);
}



void Ipp8uC1::compareC(const Ipp8uC1& src1, const IppiRect& src1Rect, Ipp8u value,
    Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp)
{
    src1.validateROI(src1Rect);


    IppiRect dstRect = MakeRect(dstPt, MakeSize(src1Rect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareC_8u_C1R(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), value,
        dst.ptr(dstPt.x, dstPt.y), dst.step(),
        MakeSize(src1Rect), cmpOp);
    ValidateStatus(status);
}



void Ipp8uC1::compareC(const IppiRect& srcRect, Ipp8u value, Ipp8uC1& dst, const IppiPoint& dstPt, IppCmpOp cmpOp) const
{
    validateROI(srcRect);

    IppiSize srcSize = MakeSize(srcRect);

    IppiRect dstRect = MakeRect(dstPt, srcSize);
    dst.validateROI(dstRect);

    IppStatus status = ippiCompareC_8u_C1R(ptr(srcRect.x, srcRect.y), step(), value,
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect), cmpOp);
    ValidateStatus(status);
}



void Ipp8uC1::countInRange(const IppiRect& rect, int& count, Ipp8u lowerBound, Ipp8u upperBound) const
{
    validateROI(rect);

    IppStatus status = ippiCountInRange_8u_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &count, lowerBound, upperBound);
    ValidateStatus(status);
}

#if IPP_VERSION_MAJOR == 7

    // TODO
#else
void Ipp8uC1::dilateBorder(const IppiRect& rect, const Ipp8uC1& mask, Ipp8uC1& dst, const IppiPoint& dstPt,
    IppiBorderType borderType, Ipp8u borderValue) const
{
    int specSize = 0;
    int bufferSize = 0;

    IppStatus status = ippiMorphologyBorderGetSize_8u_C1R(MakeSize(rect), mask.size(), &specSize, &bufferSize);
    ValidateStatus(status);

    unique_ptr<IppiMorphState, IppSignalDeleter> morphState((IppiMorphState*)ippsMalloc_8u(specSize));
    unique_ptr<Ipp8u, IppSignalDeleter> workBuffer(ippsMalloc_8u(bufferSize));

    status = ippiMorphologyBorderInit_8u_C1R(MakeSize(rect), mask.ptr(), mask.size(), morphState.get(), workBuffer.get());
    ValidateStatus(status);

    status = ippiDilateBorder_8u_C1R(ptr(rect.x, rect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(rect), borderType, borderValue,
        morphState.get(), workBuffer.get());
    ValidateStatus(status);
}

#endif

void Ipp8uC1::and(const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiAnd_8u_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp8uC1::or(const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiOr_8u_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp8uC1::xor(const IppiRect& srcDstRect, const Ipp8uC1& src, const IppiPoint& srcPt)
{
    validateROI(srcDstRect);

    IppiRect srcRect = MakeRect(srcPt, MakeSize(srcDstRect));
    src.validateROI(srcRect);

    IppStatus status = ippiXor_8u_C1IR(src.ptr(srcPt), src.step(), ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp8uC1::not(const IppiRect& srcDstRect)
{
    validateROI(srcDstRect);

    IppStatus status = ippiNot_8u_C1IR(ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}



void Ipp8uC1::andC(const IppiRect& srcDstRect, Ipp8u value)
{
    validateROI(srcDstRect);

    IppStatus status = ippiAndC_8u_C1IR(value, ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp8uC1::orC(const IppiRect& srcDstRect, Ipp8u value)
{
    validateROI(srcDstRect);

    IppStatus status = ippiOrC_8u_C1IR(value, ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}


void Ipp8uC1::xorC(const IppiRect& srcDstRect, Ipp8u value)
{
    validateROI(srcDstRect);

    IppStatus status = ippiXorC_8u_C1IR(value, ptr(srcDstRect.x, srcDstRect.y), step(), MakeSize(srcDstRect));
    ValidateStatus(status);
}





void Ipp8uC1::mean(const IppiRect& rect, Ipp64f& mean) const
{
    validateROI(rect);

    IppStatus status = ippiMean_8u_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &mean);
    ValidateStatus(status);
}



void Ipp8uC1::mean(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean) const
{
    validateROI(rect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));
    mask.validateROI(maskRect);

    IppStatus status = ippiMean_8u_C1MR(ptr(rect.x, rect.y), step(), mask.ptr(maskPt), mask.step(), MakeSize(maskRect), &mean);
    ValidateStatus(status);
}


void Ipp8uC1::meanStdDev(const IppiRect& rect, Ipp64f& mean, Ipp64f& stdDev) const
{
    validateROI(rect);

    IppStatus status = ippiMean_StdDev_8u_C1R(ptr(rect.x, rect.y), step(), MakeSize(rect), &mean, &stdDev);
    ValidateStatus(status);
}


void Ipp8uC1::meanStdDev(const IppiRect& rect, const Ipp8uC1& mask, const IppiPoint& maskPt, Ipp64f& mean, Ipp64f& stdDev) const
{
    validateROI(rect);

    IppiRect maskRect = MakeRect(maskPt, MakeSize(rect));
    mask.validateROI(maskRect);

    IppStatus status = ippiMean_StdDev_8u_C1MR(ptr(rect.x, rect.y), step(), mask.ptr(maskPt), mask.step(), MakeSize(rect), &mean, &stdDev);
    ValidateStatus(status);
}



void Ipp8uC1::qualityIndex(const IppiPoint& pt1, const Ipp8uC1& src2, const IppiRect& rect2, Ipp32f qualityIndex[1]) const
{
    src2.validateROI(rect2);
    
    IppiRect rect1 = MakeRect(pt1, MakeSize(rect2));
    validateROI(rect1);

    Ipps8u workBuff(getQualityIndexBufferSize());

    IppStatus status = ippiQualityIndex_8u32f_C1R(ptr(pt1.x, pt1.y), step_, src2.ptr(rect2.x, rect2.y), src2.step(),
        MakeSize(rect2), qualityIndex, workBuff.ptr() );
    ValidateStatus(status);
}


void Ipp8uC1::qualityIndex(const IppiPoint& pt1, const Ipp8uC1& src2, const IppiRect& rect2, Ipp32f qualityIndex[1], Ipps8u& workBuffer) const
{
    src2.validateROI(rect2);

    IppiRect rect1 = MakeRect(pt1, MakeSize(rect2));
    validateROI(rect1);

    int bufSize = getQualityIndexBufferSize();
    workBuffer.validateOffsetLength(0, bufSize);

    IppStatus status = ippiQualityIndex_8u32f_C1R(ptr(pt1.x, pt1.y), step_, src2.ptr(rect2.x, rect2.y), src2.step(),
        MakeSize(rect2), qualityIndex, workBuffer.ptr() );
    ValidateStatus(status);
}


void Ipp8uC1::qualityIndex(const Ipp8uC1& src1, const IppiPoint& pt1, const Ipp8uC1& src2, const IppiPoint& pt2, const IppiSize& size, Ipp32f qualityIndex[1])
{
    IppiRect rect1 = MakeRect(pt1, size);
    IppiRect rect2 = MakeRect(pt2, size);

    src1.validateROI(rect1);
    src2.validateROI(rect2);

    int bufferSize = 0;
    IppStatus status = ippiQualityIndexGetBufferSize(ipp8u, ippC1, size, &bufferSize);
    ValidateStatus(status);

    Ipps8u workBuffer(bufferSize);

    status = ippiQualityIndex_8u32f_C1R(src1.ptr(pt1.x, pt1.y), src1.step(), src2.ptr(rect2.x, rect2.y), src2.step(),
        MakeSize(rect2), qualityIndex, workBuffer.ptr() );
    ValidateStatus(status);
}


void Ipp8uC1::qualityIndex(const Ipp8uC1& src1, const IppiPoint& pt1, const Ipp8uC1& src2, const IppiPoint& pt2, const IppiSize& size, Ipp32f qualityIndex[1], Ipps8u& workBuff)
{
    IppiRect rect1 = MakeRect(pt1, size);
    IppiRect rect2 = MakeRect(pt2, size);

    src1.validateROI(rect1);
    src2.validateROI(rect2);

    int bufferSize = 0;
    IppStatus status = ippiQualityIndexGetBufferSize(ipp8u, ippC1, size, &bufferSize);
    ValidateStatus(status);

    workBuff.validateOffsetLength(0, bufferSize);

    status = ippiQualityIndex_8u32f_C1R(src1.ptr(pt1.x, pt1.y), src1.step(), src2.ptr(rect2.x, rect2.y), src2.step(),
        MakeSize(rect2), qualityIndex, workBuff.ptr() );
    ValidateStatus(status);
}


int Ipp8uC1::getQualityIndexBufferSize() const
{
    int bufferSize = 0;
    IppStatus status = ippiQualityIndexGetBufferSize(ipp8u, ippC1, MakeSize(width_, height_), &bufferSize);
    ValidateStatus(status);

    return bufferSize;
}



Ipp8uC1 Ipp8uC1::remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation) const
{
    Ipp8uC1 dst(width_, height_);

    remap(Origin, pxMap, Origin, pyMap, Origin, dst, Origin, size(), interpolation);

    return dst;
}



void Ipp8uC1::remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC1& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation) const
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C1R(ptr(), size, step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}



void Ipp8uC1::remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC1& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation) 
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    src.validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C1R(src.ptr(), size, src.step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}

