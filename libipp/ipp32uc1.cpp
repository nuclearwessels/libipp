
#include "ipp32uc1.h"

using namespace libipp;
using namespace std;


Ipp32uC1::Ipp32uC1(int w, int h) : IppImgBase(w, h)
{
    Ipp32u* buff = (Ipp32u*) ippiMalloc_32s_C1(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_32s_C1 failed");
    buffer_ = unique_ptr<Ipp32u, IppImgDeleter>(buff);
}


Ipp32u* Ipp32uC1::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 1);
}


Ipp32u* Ipp32uC1::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


void Ipp32uC1::convert32fC1(const Ipp32uC1& src, const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt)
{
    src.validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32u32f_C1R(src.ptr(srcRect.x, srcRect.y), src.step(), 
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect) );
    ValidateStatus(status);
}


void Ipp32uC1::convert32fC1(const IppiRect& srcRect, Ipp32fC1& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiConvert_32u32f_C1R(ptr(srcRect.x, srcRect.y), step(),
        dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


Ipp32fC1 Ipp32uC1::convert32fC1(const IppiRect& srcRect) const
{
    validateROI(srcRect);

    Ipp32fC1 dst(srcRect.width, srcRect.height);

    IppStatus status = ippiConvert_32u32f_C1R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);

    return dst;
}


