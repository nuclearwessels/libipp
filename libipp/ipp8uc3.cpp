
#include "ipp8uc3.h"

using namespace libipp;
using namespace std;


Ipp8uC3::Ipp8uC3(int w, int h) : IppImgBase(w, h)
{
    Ipp8u* buff = ippiMalloc_8u_C3(w, h, &step_);
    if (buff == 0)
        throw IppException("ippiMalloc_8u_C3 failed");
    buffer_ = unique_ptr<Ipp8u, IppImgDeleter>(buff);
}


Ipp8u* Ipp8uC3::ptr(int x, int y) const
{
    validateXY(x, y);
    return IppImgBase::ptr(x, y, 3);
}


Ipp8u* Ipp8uC3::ptr(const IppiPoint& pt) const
{
    return ptr(pt.x, pt.y);
}


void Ipp8uC3::copy(const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt) const
{
    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt, MakeSize(srcRect));
    dst.validateROI(dstRect);

    IppStatus status = ippiCopy_8u_C3R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(dstPt), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);
}


Ipp8uC3 Ipp8uC3::copy(const IppiRect& srcRect) const
{
    validateROI(srcRect);

    Ipp8uC3 dst(srcRect.width, srcRect.height);

    IppStatus status = ippiCopy_8u_C3R(ptr(srcRect.x, srcRect.y), step(), dst.ptr(), dst.step(), MakeSize(srcRect));
    ValidateStatus(status);

    return dst;
}



void Ipp8uC3::addC(Ipp8u value1, Ipp8u value2, Ipp8u value3, const IppiRect& rect, int scaleFactor)
{
    Ipp8u values[3] = { value1, value2, value3 };

    validateROI(rect);

    IppStatus status = ippiAddC_8u_C3IRSfs(values, ptr(rect.x, rect.y), step_, MakeSize(rect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC3::addC(Ipp8u value1, Ipp8u value2, Ipp8u value3, const IppiRect& srcRect, Ipp8uC3& dst, const IppiPoint& dstPt, int scaleFactor)
{
    Ipp8u values[3] = { value1, value2, value3 };

    validateROI(srcRect);

    IppiRect dstRect = MakeRect(dstPt.x, dstPt.y, srcRect.width, srcRect.height);
    dst.validateROI(dstRect);

    IppStatus status = ippiAddC_8u_C3RSfs(ptr(srcRect.x, srcRect.y), step_, values, dst.ptr(dstPt.x, dstPt.y),
        dst.step(), MakeSize(srcRect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC3::add(const IppiRect& srcDstRect, const Ipp8uC3& src2, const IppiPoint& src2Pt, int scaleFactor)
{
    validateROI(srcDstRect);

    src2.validateROI(MakeRect(src2Pt.x, src2Pt.y, srcDstRect.width, srcDstRect.height));

    IppStatus status = ippiAdd_8u_C3IRSfs(src2.ptr(src2Pt.x, src2Pt.y), src2.step(), ptr(srcDstRect.x, srcDstRect.y),
        step_, MakeSize(srcDstRect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC3::add(const Ipp8uC3& src1, const IppiRect& src1Rect, const Ipp8uC3& src2, const IppiPoint& src2Pt, Ipp8uC3& dst, const IppiPoint& dstPt, int scaleFactor)
{
    src1.validateROI(src1Rect);

    IppiRect src2Rect = MakeRect(src2Pt.x, src2Pt.y, src1Rect.width, src1Rect.height);
    src2.validateROI(src2Rect);

    IppiRect dstRect = MakeRect(dstPt.x, dstPt.y, src1Rect.width, src1Rect.height);
    dst.validateROI(dstRect);

    IppStatus status = ippiAdd_8u_C3RSfs(src1.ptr(src1Rect.x, src1Rect.y), src1.step(), src2.ptr(src2Pt.x, src2Pt.y),
        src2.step(), dst.ptr(dstPt.x, dstPt.y), dst.step(), MakeSize(src1Rect), scaleFactor);
    ValidateStatus(status);
}



void Ipp8uC3::subC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiSubC_8u_C3IRSfs(values, ptr(), step(), MakeSize(width(), height()), scaleFactor);
    ValidateStatus(status);
}



void Ipp8uC3::subC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    validateROI(rect);

    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiSubC_8u_C3IRSfs(values, ptr(rect.x, rect.y), step(), MakeSize(rect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC3::mulC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiMulC_8u_C3IRSfs(values, ptr(), step(), MakeSize(width(), height()), scaleFactor);
    ValidateStatus(status);
}



void Ipp8uC3::mulC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    validateROI(rect);

    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiMulC_8u_C3IRSfs(values, ptr(rect.x, rect.y), step(), MakeSize(rect), scaleFactor);
    ValidateStatus(status);
}


void Ipp8uC3::divC(Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiDivC_8u_C3IRSfs(values, ptr(), step(), MakeSize(width(), height()), scaleFactor);
    ValidateStatus(status);
}



void Ipp8uC3::divC(const IppiRect& rect, Ipp8u value1, Ipp8u value2, Ipp8u value3, int scaleFactor)
{
    validateROI(rect);

    Ipp8u values[3] = { value1, value2, value3 };

    IppStatus status = ippiDivC_8u_C3IRSfs(values, ptr(rect.x, rect.y), step(), MakeSize(rect), scaleFactor);
    ValidateStatus(status);
}




void Ipp8uC3::countInRange(const IppiRect & rect, int count[3], Ipp8u lowerBound[3], Ipp8u upperBound[3]) const
{
    validateROI(rect);

    IppStatus status = ippiCountInRange_8u_C3R(ptr(rect.x, rect.y), step(), MakeSize(rect), count, lowerBound, upperBound);
    ValidateStatus(status);
}



void Ipp8uC3::mirror(IppiAxis axis)
{
    IppStatus status = ippiMirror_8u_C3IR(ptr(), step(), MakeSize(width(), height()), axis);
    ValidateStatus(status);
}



void Ipp8uC3::mirror(const IppiRect& rect, IppiAxis axis)
{
    validateROI(rect);

    IppStatus status = ippiMirror_8u_C3IR(ptr(rect.x, rect.y), step(), MakeSize(rect), axis);
    ValidateStatus(status);
}



Ipp8uC3 Ipp8uC3::remap(const Ipp32fC1& pxMap, const Ipp32fC1& pyMap, int interpolation) const
{
    Ipp8uC3 dst(width_, height_);
    remap(Origin, pxMap, Origin, pyMap, Origin, dst, Origin, size(), interpolation);
    return dst;
}


void Ipp8uC3::remap(const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC3& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation) const
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C3R(ptr(), size, step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}


void Ipp8uC3::remap(const Ipp8uC1& src, const IppiPoint& srcPt, const Ipp32fC1& pxMap, const IppiPoint& pxPt,
    const Ipp32fC1& pyMap, const IppiPoint& pyPt, Ipp8uC3& dst, const IppiPoint& dstPt, IppiSize size,
    int interpolation)
{
    IppiRect srcRect = MakeRect(srcPt, size);
    IppiRect dstRect = MakeRect(dstPt, size);
    IppiRect pxRect = MakeRect(pxPt, size);
    IppiRect pyRect = MakeRect(pyPt, size);

    src.validateROI(srcRect);
    dst.validateROI(dstRect);
    pxMap.validateROI(pxRect);
    pyMap.validateROI(pyRect);

    // TODO Do I need src.ptr() or src.ptr(srcPt)
    IppStatus status = ippiRemap_8u_C3R(src.ptr(), size, src.step(), srcRect, pxMap.ptr(pxPt), pxMap.step(), pyMap.ptr(pyPt), pyMap.step(),
        dst.ptr(dstPt), dst.step(), size, interpolation);
    ValidateStatus(status);
}

