#pragma once

#include "libippclr.h"

#include "ipp8uC1clr.h"

using namespace System;
using namespace System::Runtime::InteropServices;


namespace libippclr
{

    public ref class Ipp32fC1 : IppImgBase
    {
    public:
        Ipp32fC1(int w, int h);
        Ipp32fC1(array<float, 2>^ data);
        ~Ipp32fC1();

        array<float, 2>^ ToArray(Rect^ roi);

        void Set(float value, Rect^ roi);
        void SetMask(float value, Rect^ roi, Ipp8uC1^ mask, Point^ maskPt);

        property libipp::Ipp32fC1& Native
        {
            libipp::Ipp32fC1& get() { return *ipp32fC1_; }
        }

    protected:
        !Ipp32fC1();

    private:
        libipp::Ipp32fC1* ipp32fC1_;
    };

}
