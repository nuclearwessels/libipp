// libippclr.h

#pragma once

#include "ipp.h"
#include "ipp8uc1.h"
#include "ipp32fc1.h"

using namespace System;

namespace libippclr 
{


public ref class Point
{
public:
	Point()
	{
		x_ = y_ = 0;
	}

	Point(int x, int y)
	{
		x_ = x;
		y_ = y;
	}

	property int X
	{
		int get() { return x_; }
		void set(int x) { x_ = x; }
	}

	property int Y
	{
		int get() { return y_; }
		void set(int y) { y_ = y; }
	}

	property ::IppiPoint IppiPoint
	{
		::IppiPoint get()
		{
			::IppiPoint pt;
			pt.x = x_;
			pt.y = y_;
			return pt;
		}
	}

private:
	int x_, y_;
};


public ref class Size
{
public:
	Size(int w, int h)
	{
		width_ = w;
		height_ = h;
	}

	property int Width
	{
		int get() { return width_; }
		void set(int w) { width_ = w; }
	}

	property int Height
	{
		int get() { return height_; }
		void set(int h) { height_ = h; }
	}

	property IppiSize IppiSize
	{
		::IppiSize get() 
		{
			::IppiSize sz;
			sz.width = width_;
			sz.height = height_;
			return sz;
		}
	}

private:
	int width_, height_;
};


public ref struct Rect
{
public:
	Rect(int x, int y, int width, int height)
	{
		x_ = x;
		y_ = y;
		width_ = width;
		height_ = height;
	}

	property int X
	{
		int get() { return x_; }
		void set(int x) { x_ = x;  }
	}

	property int Y
	{
		int get() { return y_; }
		void set(int y) { y_ = y; }
	}

	property int Width
	{
		int get() { return width_; }
		void set(int w) { width_ = w; }
	}

	property int Height
	{
		int get() { return height_; }
		void set(int h) { height_ = h; }
	}

	static Rect^ MakeRect(Point^ pt, Size^ size)
	{
		return gcnew Rect(pt->X, pt->Y, size->Width, size->Height);
	}

    static Rect^ MakeRect(int x, int y, int w, int h)
    {
        return gcnew Rect(x, y, w, h);
    }

	property IppiRect IppiRect
	{
		::IppiRect get()
		{
			::IppiRect r;
			r.x = x_;
			r.y = y_;
			r.width = width_;
			r.height = height_;
			return r;
		}
	}

private:
	int x_, y_;
	int width_, height_;

};


public ref class IppImgBase abstract
{
public:
	IppImgBase(int w, int h)
	{
		width_ = w;
		height_ = h;
	}

	property int Width
	{
		int get() { return width_; }
	}

	property int Height
	{
		int get() { return height_; }
	}

	void ValidateROI(Rect^ roi)
	{
		if (roi->X + roi->Width > width_)
			throw gcnew ArgumentOutOfRangeException();

		if (roi->Y + roi->Height > height_)
			throw gcnew ArgumentOutOfRangeException();
	}

protected:
	int width_, height_;
};


ref class Ipp8uC1;
ref class Ipp32fC1;


}
