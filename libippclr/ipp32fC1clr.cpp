


#include "ipp32fC1clr.h"


using namespace libippclr;


Ipp32fC1::Ipp32fC1(int w, int h) : IppImgBase(w, h)
{
    ipp32fC1_ = new libipp::Ipp32fC1(w, h);
}


Ipp32fC1::Ipp32fC1(array<float, 2>^ data) : IppImgBase(data->GetLength(0), data->GetLength(1))
{
    ipp32fC1_ = new libipp::Ipp32fC1(width_, height_);

    for (int y = 0; y < height_; y++)
    {
        Ipp32f* p = ipp32fC1_->ptr(0, y);

        for (int x = 0; x < width_; x++)
        {
            *p = data[x, y];
            p++;
        }
    }
}


libippclr::Ipp32fC1::~Ipp32fC1()
{
    if (ipp32fC1_)
    {
        delete ipp32fC1_;
        ipp32fC1_ = 0;
    }
}


array<float, 2>^ Ipp32fC1::ToArray(Rect^ roi)
{
    ValidateROI(roi);

    array<float, 2>^ data = gcnew array<float, 2>(roi->Width, roi->Height);

    for (int y = 0; y < roi->Height; y++)
    {
        Ipp32f* p = ipp32fC1_->ptr(roi->X, roi->Y + y);
        for (int x = 0; x < roi->Width; x++)
        {
            data[x, y] = *p;
            p++;
        }
    }
    return data;
}



void libippclr::Ipp32fC1::Set(float value, Rect^ roi)
{
    ipp32fC1_->set(value, roi->IppiRect);
}


void libippclr::Ipp32fC1::SetMask(float value, Rect^ roi, Ipp8uC1 ^ mask, Point ^ maskPt)
{
    ipp32fC1_->setMask(value, roi->IppiRect, mask->Native, maskPt->IppiPoint);
}



libippclr::Ipp32fC1::!Ipp32fC1()
{
    if (ipp32fC1_)
    {
        delete ipp32fC1_;
        ipp32fC1_ = 0;
    }
}


