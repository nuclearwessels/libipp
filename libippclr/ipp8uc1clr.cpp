

#include "ipp8uC1clr.h"

using namespace libippclr;


Ipp8uC1::Ipp8uC1(int w, int h) : IppImgBase(w, h)
{
	ipp8uC1_ = new libipp::Ipp8uC1(w, h);
}


Ipp8uC1::Ipp8uC1(array<Byte, 2>^ data) : IppImgBase(data->GetLength(0), data->GetLength(1))
{
	ipp8uC1_ = new libipp::Ipp8uC1(width_, height_);

	for (int y = 0; y < height_; y++)
	{
		Ipp8u* p = ipp8uC1_->ptr(0, y);
		for (int x = 0; x < width_; x++)
		{
			*p = data[x, y];
			p++;
		}
	}
}


libippclr::Ipp8uC1::!Ipp8uC1()
{
    if (ipp8uC1_)
    {
        delete ipp8uC1_;
        ipp8uC1_ = 0;
    }
}


libippclr::Ipp8uC1::~Ipp8uC1()
{
	if (ipp8uC1_)
	{
		delete ipp8uC1_;
		ipp8uC1_ = 0;
	}
}


array<Byte, 2>^ Ipp8uC1::ToArray()
{
    return ToArray(Rect::MakeRect(0, 0, Width, Height));
}


array<Byte, 2>^ Ipp8uC1::ToArray(Rect^ roi)
{
	ValidateROI(roi);

	array<Byte, 2>^ data = gcnew array<Byte, 2>(roi->Width, roi->Height);

	for (int y = 0; y < roi->Height; y++)
	{
		Ipp8u* p = ipp8uC1_->ptr(roi->X, roi->Y + y);
		for (int x = 0; x < roi->Width; x++)
		{
			data[x, y] = *p;
			p++;
		}
	}
	return data;
}



void libippclr::Ipp8uC1::Set(Byte value, Rect^ roi)
{
	ipp8uC1_->set(value, roi->IppiRect);
}

void libippclr::Ipp8uC1::SetMask(Byte value, Rect^ roi, Ipp8uC1 ^ mask, Point ^ maskPt)
{
	ipp8uC1_->setMask(value, roi->IppiRect, mask->Native, maskPt->IppiPoint);
}



void libippclr::Ipp8uC1::Convert32fC1(Rect^ srcRect, Ipp32fC1^ dst, Point^ dstPt)
{
    IppiRect ippiSrcRect = srcRect->IppiRect;
    IppiPoint ippiDstPt = dstPt->IppiPoint;

    ipp8uC1_->convert32fC1(ippiSrcRect, dst->Native, ippiDstPt);
}