#pragma once


#include "libippclr.h"

#include "ipp32fC1clr.h"

using namespace System;
using namespace System::Runtime::InteropServices;


namespace libippclr
{

public ref class Ipp8uC1 : IppImgBase
{
public:
	Ipp8uC1(int w, int h);
	Ipp8uC1(array<Byte, 2>^ data);
	~Ipp8uC1();

    array<Byte, 2>^ ToArray();
	array<Byte, 2>^ ToArray(Rect^ roi);

	void Set(Byte value, Rect^ roi);
	void SetMask(Byte value, Rect^ roi, Ipp8uC1^ mask, Point^ maskPt);

	//void convert16uC1(const IppiRect& srcRect, Ipp16uC1& dst, const IppiPoint& dstPt) const;
	//void convert16sC1(const IppiRect& srcRect, Ipp16sC1& dst, const IppiPoint& dstPt) const;
	//void convert32sC1(const IppiRect& srcRect, Ipp32sC1& dst, const IppiPoint& dstPt) const;
	void Convert32fC1(Rect^ srcRect, Ipp32fC1^ dst, Point^ dstPt);

	property libipp::Ipp8uC1& Native
	{
		libipp::Ipp8uC1& get() { return *ipp8uC1_;  }
	}

protected:
	!Ipp8uC1();

private:
	libipp::Ipp8uC1* ipp8uC1_;
};

}
