﻿using System;

using libippclr;
using NUnit.Framework;

namespace libippclr_test
{

    [TestFixture]
    public class Ipp8uC1Test
    {
        [Test]
        public void MallocAndToArray()
        {
            byte[,] data = new byte[20, 10];
            int counter = 0;
            for (int y = 0; y < data.GetLength(1); y++)
            {
                for (int x = 0; x < data.GetLength(0); x++)
                {
                    data[x, y] = (byte)counter++;
                }
            }

            Ipp8uC1 img = new Ipp8uC1(data);

            byte[,] data2 = img.ToArray(Rect.MakeRect(0, 0, img.Width, img.Height));

            for (int y=0; y<data.GetLength(1); y++)
            {
                for (int x=0; x<data.GetLength(0); x++)
                {
                    Assert.AreEqual(data[x, y], data2[x, y]);
                }
            }
        }
    }


}
