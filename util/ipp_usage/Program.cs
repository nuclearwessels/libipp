﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ipp_usage
{
    class Program
    {
        static void Main(string[] args)
        {
            Regex regex = new Regex(@"(ippi.*)\(.*\)");

            using (StreamWriter sw = File.CreateText("ipp_usage.csv"))
            {
                using (StreamReader sr = File.OpenText(@"..\..\..\..\ipp_usage2.txt"))
                {
                    string line = sr.ReadLine();

                    while (line != null)
                    {   
                        string function;

                        if (regex.IsMatch(line))
                        {
                            Match match = regex.Match(line);
                            function = match.Groups[1].Value;
                        }
                        else
                            function = String.Empty;

                        sw.WriteLine($"{function},\"{line}\"");

                        line = sr.ReadLine();
                    }

                }
            }
        }
    }
}
